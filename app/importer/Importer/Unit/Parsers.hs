{-# OPTIONS_GHC -fno-warn-orphans #-}

module Importer.Unit.Parsers where

import Control.Lens ((<&>))
import qualified Data.ByteString as B
import Data.Csv
import Data.Int (Int32, Int64)
import Data.Maybe (catMaybes, fromMaybe)
import Data.Text (Text)
import Database.Model.MagicPathUnit
import Database.Model.Unit
import Importer.MagicPathUnit
import Importer.Unit.UnitId

type DescLessBasicUnit = (UnitId, Maybe Text -> BasicUnitAttributes)

type UnitRowParser =
  ( DescLessBasicUnit,
    LeadershipAttributes,
    [UnitSpecialAbility],
    [MPLessMPU] -- can be empty
  )

instance FromNamedRecord UnitRowParser where
  parseNamedRecord r = do
    bu <- parseNamedRecord r
    ca <- parseNamedRecord r
    uas <- parseNamedRecord r
    mpus <- parseNamedRecord r
    return (bu, ca, uas, mpus)

instance FromNamedRecord DescLessBasicUnit where
  parseNamedRecord r = do
    pid <- getId r
    pname <- r .: "name"
    phitPoints <- r .: "hp"
    psize <- r .: "size"
    pstrength <- r .: "str"
    pattach <- r .: "att"
    pdefense <- r .: "def"
    pprotection <- r .: "prot"
    pmorale <- r .: "mor"
    pmagicResistance <- r .: "mr"
    pprecision <- r .: "prec"
    pencumbrance <- r .: "enc"
    pmapMove <- r .: "mapmove"
    pcombatSpeed <- r .: "ap"
    pstartingAge <- negate . fromMaybe 1 <$> (r .: "startage")
    return
      ( pid,
        BasicUnitAttributes
          pid
          pname
          phitPoints
          psize
          pstrength
          pattach
          pdefense
          pprotection
          pmorale
          pmagicResistance
          pprecision
          pencumbrance
          pmapMove
          pcombatSpeed
          pstartingAge
      )

instance FromNamedRecord LeadershipAttributes where
  parseNamedRecord r =
    LeadershipAttributes
      <$> getId r
      <*> r .: "leader"
      <*> (fromMaybe 0 <$> (r .: "undeadleader"))
      <*> (fromMaybe 0 <$> (r .: "magicleader"))

----------------
-- Special Abilities

defaultSailingMaxUnitSize :: Int64 -- TODO Move this to a central place
defaultSailingMaxUnitSize = 6 -- max size

namedRecordtoAttribute :: NamedRecord -> SpecialAbilityClass -> B.ByteString -> Parser (Maybe SpecialAbility)
namedRecordtoAttribute r spClass field = do
  v <- r .: field
  return $
    if v == Just (1 :: Int32)
      then Just (SpecialAbility spClass (-1))
      else Nothing

instance FromNamedRecord [UnitSpecialAbility] where
  parseNamedRecord r = do
    uid <- getId r

    let toSimpleUnitAttribute (n, f) = do
          attM <- namedRecordtoAttribute r n f
          return $ fmap (\att -> UnitSpecialAbility uid att Nothing Nothing) attM

    simpleSAs <-
      mapM
        toSimpleUnitAttribute
        [ (Undead, "undead"),
          (Inanimate, "inanimate"),
          (Demon, "demon")
        ]

    sailingMaxUnitSize :: Int64 <- fromMaybe defaultSailingMaxUnitSize <$> (r .: "sailingmaxunitsize")
    sailingShipSizeM :: Maybe Int64 <- r .: "sailingshipsize"
    let sailing =
          sailingShipSizeM <&> \sailingShipSize ->
            UnitSpecialAbility uid (SpecialAbility Sailing 2) (Just sailingShipSize) (Just sailingMaxUnitSize)

    return . catMaybes $ sailing : simpleSAs
