# Spells

Numbers are given for IDs of specific spells (to give examples)

## Missing fields/things


  - [ ] effects

    Spells have effects (like protection or other
    things).

    - gamedata/effect_modifier_bits.csv have info on some effects
      (all?)

  - [ ] Summons

    Some spells summon units, and somehow unit information is included
    (probably via effects)

    - Number of summons in the Inspector is shown (in advanced mode)
      with things like `5+[1/2 lvl]` (sometimes, see `490`)

  - [ ] Information on ritual/combat spells

    Spells can be ritual or combat spells, but it's not clear yet how.

  - [ ] Source terrain

    Some spells have restrictions on terrain

  - [ ] May use underwater [uwok]; use underwater only [owonly]

    Some spells may be used underwater, but most can't. Some can only
    be cast underwater, but it seems that's an additional "mark" or
    effect.

  - [ ] Damage

    Sometimes damage shown is not the one that is registered for the
    spell, eg. 163, 204.

  - [X] National spells

    Some spells are only related to some nations.

    - gamedata/attribute_keys.csv has this information:


## Attributes

  gamedata/attribute_keys.csv has information on attributes for
  spells, marked `Spl`.

  The following attributes are found:

```
278     Restrict to Nation {Spl: #restricted}
700     Map Range {Spl: #provrange}
701     Destination Terrain {Spl: #onlygeodst}
702     Source Terrain {Spl: #onlygeosrc}
703     Cannot target geo mask {Spl: #nogeodst}
705     Cannot trace range through seas {Spl: #nowatertrace}
706     No Path over Land {Spl: #nolandtrace}
708     Hidden Enchantment {Spl: #hiddenench}
713     Only cast at coast {Spl: #onlycoastsrc}
716     Remote summon commander {Spl: #farsumcom}
722     God Path Restriction {Spl: #godpathspell}
723     Casting Time {Spl: #casttime}
724     Extra effect in geo {Spl: #extraeffectgeo}
725     Can't target same province more than once/turn {Spl: #uniquetarget}
728     Adds AN lingering heat {Spl: #anlingeringheat}
729     Dispelled if province changes owners {Spl: #friendlyench}
731     Can only be cast by {Spl: #onlymnr}
732     Unit gets magic of target {Spl: #polygetmagic}
733     Sets current province as home {Spl: #sethome}
738     Only target friendly provinces {Spl: #onlyowndst}
``` 
