module Database.Model.Spell where

import Data.Int (Int32, Int64)
import Data.Profunctor.Product.TH (makeAdaptorAndInstance)
import Data.Hashable
import Data.Text
import Opaleye

data SpellT sId text smallInt bigInt maybeText = Spell
  { spellId :: sId,
    name :: text,
    fatigueCost :: smallInt,
    gemCost :: smallInt,
    damage :: bigInt,
    precision :: smallInt,
    description :: maybeText,
    details :: maybeText
  }
  deriving (Show)

newtype SpellIdT idInt = SpellId idInt
  deriving (Show, Eq, Ord)

type SpellId = SpellIdT Int64

$(makeAdaptorAndInstance "pSpellId" ''SpellIdT)

instance Hashable SpellId where
  hashWithSalt s (SpellId a) = hashWithSalt s a

type Spell =
  SpellT SpellId Text Int32 Int64 (Maybe Text)

$(makeAdaptorAndInstance "pSpell" ''SpellT)

type FieldSpellId = SpellIdT (Field SqlInt8)

type SpellField =
  SpellT
    FieldSpellId
    (Field SqlText)
    (Field SqlInt4)
    (Field SqlInt8)
    (FieldNullable SqlText)

spellsTable :: Table SpellField SpellField
spellsTable =
  table
    "spells"
    ( pSpell
        Spell
          { spellId = pSpellId (SpellId (tableField "id")),
            name = tableField "name",
            fatigueCost = tableField "fatigue_cost",
            gemCost = tableField "gem_cost",
            damage = tableField "damage",
            precision = tableField "precision",
            description = tableField "description",
            details = tableField "details"
          }
    )

selectSpells :: Select SpellField
selectSpells = selectTable spellsTable
