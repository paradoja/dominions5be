{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}

module Database.Model.Unit.BasicUnitAttributes where

import Control.Lens.TH
import Data.Int (Int32)
import Data.Profunctor.Product.TH (makeAdaptorAndInstanceInferrable)
import Data.Text (Text)
import Database.Model.Unit.Utils
import Opaleye (Field, FieldNullable, Select, SqlInt4, SqlText, Table, selectTable, table, tableField)

data BasicUnitAttributesT id text smallint maybeText = BasicUnitAttributes
  { _unitId :: id,
    _name :: text,
    _hitPoints :: smallint,
    _size :: smallint,
    _strength :: smallint,
    _attack :: smallint,
    _defense :: smallint,
    _protection :: smallint,
    _morale :: smallint,
    _magicResistance :: smallint,
    _precision :: smallint,
    _encumbrance :: smallint,
    _mapMove :: smallint,
    _combatSpeed :: smallint,
    _startingAge :: smallint,
    _description :: maybeText
  }
  deriving (Show)

$(makeLenses ''BasicUnitAttributesT)
$(makeAdaptorAndInstanceInferrable "pBasicUnitAttributes" ''BasicUnitAttributesT)

type BasicUnitAttributes = BasicUnitAttributesT UnitId Text Int32 (Maybe Text)

type BasicUnitAttributesField =
  BasicUnitAttributesT FieldUnitId (Field SqlText) (Field SqlInt4) (FieldNullable SqlText)

basicUnitAttributesTable :: Table BasicUnitAttributesField BasicUnitAttributesField
basicUnitAttributesTable =
  table
    "units"
    ( pBasicUnitAttributes
        BasicUnitAttributes
          { _unitId = pUnitId (UnitId (tableField "id")),
            _name = tableField "name",
            _hitPoints = tableField "hit_points",
            _size = tableField "size",
            _strength = tableField "strength",
            _attack = tableField "attack",
            _defense = tableField "defense",
            _protection = tableField "protection",
            _morale = tableField "morale",
            _magicResistance = tableField "magic_resistance",
            _precision = tableField "precision",
            _encumbrance = tableField "encumbrance",
            _mapMove = tableField "map_move",
            _combatSpeed = tableField "combat_speed",
            _startingAge = tableField "starting_age",
            _description = tableField "description"
          }
    )

selectBasicUnitAttributes :: Select BasicUnitAttributesField
selectBasicUnitAttributes = selectTable basicUnitAttributesTable
