-- migrate:up
CREATE TABLE units (
       id integer PRIMARY KEY,
       name character varying NOT NULL,
       hit_points smallint NOT NULL,
       size smallint NOT NULL,
       strength smallint NOT NULL,
       attack smallint NOT NULL,
       defense smallint NOT NULL,
       protection smallint NOT NULL,
       morale smallint NOT NULL,
       magic_resistance smallint NOT NULL,
       precision smallint NOT NULL,
       encumbrance smallint NOT NULL,
       map_move smallint NOT NULL,
       combat_speed smallint NOT NULL,
       starting_age smallint NOT NULL
);

CREATE TABLE commander_attributes (
       unit_id integer
         PRIMARY KEY
         REFERENCES units
         ON DELETE CASCADE,

       leadership smallint NOT NULL,
       undead_leadership smallint NOT NULL,
       magical_leadership smallint NOT NULL
);
-- migrate:down

DROP TABLE commander_attributes;
DROP TABLE units;
