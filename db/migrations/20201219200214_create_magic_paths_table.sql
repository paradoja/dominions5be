-- migrate:up
CREATE TABLE magic_paths (
    id integer PRIMARY KEY,
    name character varying NOT NULL
);

-- migrate:down

DROP TABLE magic_paths;
