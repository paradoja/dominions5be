{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeFamilies #-}

module Api.Schema where

import Control.Lens.TH
import Data.Morpheus.Document (importGQLDocument)
import Data.Morpheus.Types
import Data.Text (Text)
import Datasource (Haxl)

importGQLDocument "schema.graphql"

type MQueryF a = Resolver QUERY a Haxl
