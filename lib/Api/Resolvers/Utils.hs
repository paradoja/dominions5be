module Api.Resolvers.Utils where

import Data.Int (Int64)
import Data.Morpheus.Types
import Data.Text.Read (decimal)

idFromGraphQL :: ID -> Either String Int64
idFromGraphQL graphqlId =
  either
    (Left . ("Invalid ID : " ++))
    (Right . fst)
    (decimal $ unpackID graphqlId)
