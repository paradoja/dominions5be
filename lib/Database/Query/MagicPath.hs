module Database.Query.MagicPath
  ( selectMagicPaths,
  )
where

import Database.Model.MagicPath
import Opaleye

selectMagicPaths :: Select MagicPathField
selectMagicPaths = selectTable magicPathsTable
