module Database.Mutations.Unit where

import Control.Lens (preview, view)
import Data.Int (Int64)
import Data.Maybe (mapMaybe)
import Database.Model.Unit
import qualified Database.Mutations.Unit.BasicUnitAttributes as BU
import qualified Database.Mutations.Unit.LeadershipAttributes as CA
import qualified Database.Mutations.Unit.UnitSpecialAbility as UA
import Opaleye (Delete, Insert)

insert :: [Unit] -> [Insert Int64]
insert objs =
  let bus = fmap (view basicUnitAttributes) objs
      cas = mapMaybe (preview leadershipAttributes) objs
      uas = concatMap (view specialAbilities) objs
   in [ BU.insert bus,
        CA.insert cas,
        UA.insert uas
      ]

deleteAll :: [Delete Int64]
deleteAll = [BU.deleteAll, CA.deleteAll, BU.deleteAll]
