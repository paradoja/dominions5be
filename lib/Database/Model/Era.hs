{-# LANGUAGE TypeFamilies #-}

module Database.Model.Era where

import Data.Hashable
import qualified Data.Profunctor.Product.Default as D
import qualified Opaleye as O
import Opaleye.Experimental.Enum (fromFieldToFieldsEnum)
import Opaleye.Internal.Inferrable

data Era
  = Early
  | Middle
  | Late
  deriving (Show, Eq, Ord)

data SqlEra

instance Hashable Era where
  hashWithSalt i e = hashWithSalt i (show e)

toSqlEraString :: Era -> String
toSqlEraString Early = "early"
toSqlEraString Middle = "middle"
toSqlEraString Late = "late"

fromSqlEraString :: String -> Maybe Era
fromSqlEraString "early" = Just Early
fromSqlEraString "middle" = Just Middle
fromSqlEraString "late" = Just Late
fromSqlEraString _ = Nothing

fromFieldEra :: O.FromField sqlEnum Era
toFieldsEra :: O.ToFields Era (O.Column sqlEnum)
(fromFieldEra, toFieldsEra) =
  fromFieldToFieldsEnum "era" fromSqlEraString toSqlEraString

instance O.DefaultFromField SqlEra Era where
  defaultFromField = fromFieldEra

instance
  era ~ Era =>
  D.Default (Inferrable O.FromFields) (O.Column SqlEra) era
  where
  def = Inferrable D.def

instance D.Default O.ToFields Era (O.Column SqlEra) where
  def = toFieldsEra
