module Database.Model.Unit.UnitSpecialAbility where

import Control.Lens.TH
import Data.Int (Int64)
import Data.Profunctor.Product.TH (makeAdaptorAndInstance)
import qualified Database.Model.Unit.SpecialAbility as UA
import Database.Model.Unit.Utils
import Opaleye
  ( Field,
    FieldNullable,
    Select,
    SqlInt8,
    Table,
    selectTable,
    table,
    tableField,
  )

data UnitSpecialAbilityT uid spAbility maybeBigInt = UnitSpecialAbility
  { _unitId :: uid,
    _specialAbility :: spAbility,
    _value1 :: maybeBigInt,
    _value2 :: maybeBigInt
  }
  deriving (Show)

$(makeLenses ''UnitSpecialAbilityT)
$(makeAdaptorAndInstance "pSpecialAbility" ''UnitSpecialAbilityT)

type UnitSpecialAbility =
  UnitSpecialAbilityT UnitId UA.SpecialAbility (Maybe Int64)

type UnitSpecialAbilityField =
  UnitSpecialAbilityT
    FieldUnitId
    (Field UA.SqlSpecialAbility)
    (FieldNullable SqlInt8)

unitSpecialAbilitiesTable :: Table UnitSpecialAbilityField UnitSpecialAbilityField
unitSpecialAbilitiesTable =
  table
    "unit_special_abilities"
    ( pSpecialAbility
        UnitSpecialAbility
          { _unitId = pUnitId (UnitId (tableField "unit_id")),
            _specialAbility = tableField "special_ability",
            _value1 = tableField "value1",
            _value2 = tableField "value2"
          }
    )

selectUnitSpecialAbilities :: Select UnitSpecialAbilityField
selectUnitSpecialAbilities = selectTable unitSpecialAbilitiesTable
