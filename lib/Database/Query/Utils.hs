module Database.Query.Utils where

import Data.Text as T (Text, cons, snoc, unwords, words)
import Opaleye

maybeFilterByNameFuzzy :: Column SqlText -> Maybe Text -> SelectArr () ()
maybeFilterByNameFuzzy name =
  maybeFilterByName name . fmap (\n -> '%' `cons` (T.unwords . T.words $ n) `snoc` '%')

maybeFilterByName :: Column SqlText -> Maybe Text -> SelectArr () ()
maybeFilterByName name =
  maybe
    (return ())
    (\n -> viaLateral restrict (name `ilike` toFields n))
