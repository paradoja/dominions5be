module Utils where

import Configuration.Dotenv (defaultConfig, loadFile)
import Data.ByteString.Char8 (ByteString, pack)
import System.Environment (getEnv)

buildConnectionString :: [(String, String)] -> ByteString
buildConnectionString =
  pack
    . unwords
    . map (\(a, b) -> a ++ "=" ++ b)

getDBInfo :: IO [(String, String)]
getDBInfo = do
  -- if doenv is present, load it, but respect curren env variables
  _ <- loadFile defaultConfig

  host <- getEnv "PGHOST"
  dbport <- getEnv "PGPORT"
  dbname <- getEnv "PGDATABASE"
  dbuser <- getEnv "PGUSER"
  dbpwd <- getEnv "PGPASS"
  return
    [ ("host", host),
      ("port", dbport),
      ("dbname", dbname),
      ("user", dbuser),
      ("password", dbpwd)
    ]

getConnectionString :: IO ByteString
getConnectionString = fmap buildConnectionString getDBInfo
