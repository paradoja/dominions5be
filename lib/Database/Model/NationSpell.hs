module Database.Model.NationSpell where

import Data.Profunctor.Product.TH (makeAdaptorAndInstance)
import Database.Model.Nation
import Database.Model.Spell
import Opaleye

data NationSpellT a b = NationSpell
  { spellId :: a,
    nationId :: b
  }
  deriving (Show)

type NationSpell =
  NationSpellT SpellId NationId

$(makeAdaptorAndInstance "pNationSpell" ''NationSpellT)

type NationSpellField =
  NationSpellT
    FieldSpellId
    FieldNationId

nationSpellTable :: Table NationSpellField NationSpellField
nationSpellTable =
  table
    "nation_spell"
    ( pNationSpell
        NationSpell
          { spellId = pSpellId (SpellId (tableField "spell_id")),
            nationId = pNationId (NationId (tableField "nation_id"))
          }
    )

selectNationSpells :: Select NationSpellField
selectNationSpells = selectTable nationSpellTable
