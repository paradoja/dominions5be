{-# LANGUAGE TypeFamilies #-}

module Datasource.Database where

import Control.Exception
import Control.Lens
import Control.Monad
import Data.Hashable
import Data.Int (Int32)
import qualified Data.Map as Map
import Data.Maybe
import Data.Pool (Pool, withResource)
import Data.Profunctor.Product.Default (Default)
import Data.Text (Text)
import Data.Tuple.Curry (uncurryN)
import Data.Typeable
import Database.Model
import Database.PostgreSQL.Simple (Connection)
import Database.Query
import Haxl.Core
import Language.Haskell.TH
import Opaleye

----------------
-- Request type

data DBRequest a where
  FetchBasicUnit :: UnitId -> DBRequest BasicUnit
  FetchBasicUnits :: Maybe Text -> DBRequest [BasicUnit]
  FetchUnitSpecialAbilities :: UnitId -> DBRequest [UnitSpecialAbility]
  FetchUnitMagicPathLevels :: UnitId -> DBRequest [MagicPathLevel]
  FetchSpellsAndMagicSchools ::
    Maybe NationId ->
    Maybe Text ->
    Maybe Text ->
    Bool ->
    DBRequest [(Spell, Maybe (MagicSchool, Int32))]
  FetchMagicPathsForSpell :: SpellId -> DBRequest [(MagicPath, Int32)]
  FetchRestrictedNationsForSpell :: SpellId -> DBRequest [Nation]
  FetchNation :: NationId -> DBRequest Nation
  FetchNations :: Maybe Era -> DBRequest [Nation]
  FetchMagicPaths :: DBRequest [MagicPath]
  deriving (Typeable)

deriving instance Show (DBRequest a)

instance ShowP DBRequest where showp = show

deriving instance Eq (DBRequest a)

instance Hashable (DBRequest a) where
  -- units 0s
  hashWithSalt s (FetchBasicUnit uid) = hashWithSalt s (0 :: Int, uid)
  hashWithSalt s (FetchBasicUnits nameM) = hashWithSalt s (1 :: Int, nameM)
  hashWithSalt s (FetchUnitSpecialAbilities uid) = hashWithSalt s (2 :: Int, uid)
  hashWithSalt s (FetchUnitMagicPathLevels uid) = hashWithSalt s (3 :: Int, uid)
  -- spells 100s
  hashWithSalt s (FetchSpellsAndMagicSchools nid sname school internal) =
    hashWithSalt s (100 :: Int, nid, sname, school, internal)
  hashWithSalt s (FetchMagicPathsForSpell sid) = hashWithSalt s (101 :: Int, sid)
  hashWithSalt s (FetchRestrictedNationsForSpell sid) = hashWithSalt s (102 :: Int, sid)
  -- nations 200s
  hashWithSalt s (FetchNation n) = hashWithSalt s (200 :: Int, n)
  hashWithSalt s (FetchNations eM) = hashWithSalt s (201 :: Int, eM)
  -- magicpaths 300s
  hashWithSalt s FetchMagicPaths = hashWithSalt s (300 :: Int)

----------------
-- DataSource

data Batches = Batches
  { _fetchBasicUnit :: [(UnitId, ResultVar BasicUnit)],
    _fetchBasicUnits :: [(Maybe Text, ResultVar [BasicUnit])],
    _fetchUnitSpecialAbilities :: [(UnitId, ResultVar [UnitSpecialAbility])],
    _fetchUnitMagicPathLevels :: [(UnitId, ResultVar [MagicPathLevel])],
    _fetchSpellsAndMagicSchools ::
      [ ( (Maybe NationId, Maybe Text, Maybe Text, Bool),
          ResultVar [(Spell, Maybe (MagicSchool, Int32))]
        )
      ],
    _fetchMagicPathsForSpell :: [(SpellId, ResultVar [(MagicPath, Int32)])],
    _fetchRestrictedNationsForSpell :: [(SpellId, ResultVar [Nation])],
    _fetchNation :: [(NationId, ResultVar Nation)],
    _fetchNations :: [(Maybe Era, ResultVar [Nation])],
    _fetchMagicPaths :: [((), ResultVar [MagicPath])]
  }

$(makeLenses ''Batches)

instance StateKey DBRequest where
  data State DBRequest = DBState
    { pool :: Pool Connection
    }

instance DataSourceName DBRequest where
  dataSourceName _ = "DBRequest"

instance DataSource u DBRequest where
  fetch (DBState pool) _flags _userEnv = SyncFetch $ batchFetch pool

emptyBatches :: Batches
-- initialize all to []
emptyBatches = uncurryN Batches $(tupE $ concat $ replicate 10 [[|[]|]])

collect :: BlockedFetch DBRequest -> Batches -> Batches
collect (BlockedFetch (FetchBasicUnit uid) v) = over fetchBasicUnit ((uid, v) :)
collect (BlockedFetch (FetchBasicUnits nameM) v) = over fetchBasicUnits ((nameM, v) :)
collect (BlockedFetch (FetchUnitSpecialAbilities uid) v) = over fetchUnitSpecialAbilities ((uid, v) :)
collect (BlockedFetch (FetchUnitMagicPathLevels uid) v) = over fetchUnitMagicPathLevels ((uid, v) :)
collect (BlockedFetch (FetchSpellsAndMagicSchools nid sname school internal) v) =
  over fetchSpellsAndMagicSchools (((nid, sname, school, internal), v) :)
collect (BlockedFetch (FetchMagicPathsForSpell sid) v) = over fetchMagicPathsForSpell ((sid, v) :)
collect (BlockedFetch (FetchRestrictedNationsForSpell sid) v) = over fetchRestrictedNationsForSpell ((sid, v) :)
collect (BlockedFetch (FetchNation nid) v) = over fetchNation ((nid, v) :)
collect (BlockedFetch (FetchNations eraM) v) = over fetchNations ((eraM, v) :)
collect (BlockedFetch FetchMagicPaths v) = over fetchMagicPaths (((), v) :)

batchFetch :: Pool Connection -> [BlockedFetch DBRequest] -> IO ()
batchFetch p = doFetch p . foldr collect emptyBatches

doFetch :: Pool Connection -> Batches -> IO ()
doFetch p b = do
  dbGroupedFetch
    p
    (b ^. fetchBasicUnit)
    selectBasicUnitByIds
    Map.fromList
    Map.lookup
    "unit(id)"

  dbUngroupedFetch
    p
    (b ^. fetchBasicUnits)
    selectBasicUnitsForName
    (\_ x -> x)
    (Just . fromMaybe [])
    "units(name?)"

  dbGroupedFetch
    p
    (b ^. fetchUnitSpecialAbilities)
    selectUnitSpecialAbilitiesByIds
    (Map.fromListWith (++) . reverse . fmap (over _2 (: [])))
    (\k -> Just . Map.findWithDefault [] k)
    "unitSpecialAbilities(id)"

  dbGroupedFetch
    p
    (b ^. fetchUnitMagicPathLevels)
    selectUnitMagicPathLevelsByIds
    (Map.fromListWith (++) . reverse . fmap (over _2 (: [])))
    (\k -> Just . Map.findWithDefault [] k)
    "unitMagicPathLevels(id)"

  dbUngroupedFetch
    p
    (b ^. fetchSpellsAndMagicSchools)
    (uncurryN selectSpellsAndMagicSchoolLevels)
    (,)
    (Just . fromMaybe [])
    "spells(nationId?,name?,school?,internal)"

  dbGroupedFetch
    p
    (b ^. fetchMagicPathsForSpell)
    selectPathsForSpells
    (Map.fromListWith (++) . reverse . fmap (\(s, mp, l) -> (s, [(mp, l)])))
    (\k -> Just . Map.findWithDefault [] k)
    "magicPathsForSpell(id)"

  dbGroupedFetch
    p
    (b ^. fetchRestrictedNationsForSpell)
    selectRestrictedNationsForSpells
    (Map.fromListWith (++) . reverse . fmap (over _2 (: [])))
    (\k -> Just . Map.findWithDefault [] k)
    "restrictedNationsForSpell(id)"

  dbGroupedFetch
    p
    (b ^. fetchNation)
    selectNationByIds
    Map.fromList
    Map.lookup
    "nation(id)"

  dbUngroupedFetch
    p
    (b ^. fetchNations)
    selectNationsFromEra
    (,)
    (Just . fromMaybe [])
    "nations(era?)"

  dbGroupedFetch
    p
    (b ^. fetchMagicPaths)
    (const selectMagicPaths)
    id
    (const Just)
    "magicPaths()"

dbGroupedFetch ::
  ( Ord valIn,
    Show valIn,
    Show result,
    Default FromFields resFields intResult,
    Default Unpackspec resFields resFields
  ) =>
  Pool Connection ->
  [(valIn, ResultVar result)] ->
  ([valIn] -> Select resFields) ->
  ([intResult] -> collection) ->
  (valIn -> collection -> Maybe result) ->
  String ->
  IO ()
dbGroupedFetch p requests select collate extract fecthName =
  dbFetch requests collate extract $
    withResource p $ \c -> do
      putStrLn $ fecthName ++ ": " ++ show (fmap fst requests)
      --printSql select
      runSelect c $ select (fst <$> requests)

dbUngroupedFetch ::
  ( Ord valIn,
    Show valIn,
    Show result,
    Default FromFields haskFields hask,
    Default Unpackspec haskFields haskFields
  ) =>
  Pool Connection ->
  [(valIn, ResultVar result)] ->
  (valIn -> Select haskFields) ->
  (valIn -> hask -> (valIn, key)) ->
  (Maybe [key] -> Maybe result) ->
  String ->
  IO ()
dbUngroupedFetch p requests select preCollate postExtract fetchName =
  dbFetch
    requests
    (Map.fromListWith (++) . reverse . fmap (over _2 (: [])) . concat)
    (\k -> postExtract . Map.lookup k)
    $ forM requests $ \(req, _) -> withResource p $ \c -> do
      putStrLn $ fetchName ++ ": " ++ show req
      -- printSql $ select req
      res <- runSelect c $ select req
      return $ preCollate req <$> res

dbFetch ::
  (Show valIn, Show result) =>
  [(valIn, ResultVar result)] ->
  (intResults -> collection) ->
  (valIn -> collection -> Maybe result) ->
  IO intResults ->
  IO ()
dbFetch [] _ _ _ = return ()
dbFetch requests collate extract getResults = do
  results <- getResults
  let fetched = collate results
  forM_ requests $ \req ->
    case extract (fst req) fetched of
      Nothing -> putFailure (snd req) DBNotFound
      Just r -> putSuccess (snd req) r

----------------
-- Exceptions

data DBNotFound = DBNotFound
  deriving (Show, Typeable)

instance Exception DBNotFound where
  toException = transientErrorToException
  fromException = transientErrorFromException

printSql :: Default Unpackspec a a => Select a -> IO ()
printSql = putStrLn . fromMaybe "Empty select" . showSql
