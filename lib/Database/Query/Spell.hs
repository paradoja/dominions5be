module Database.Query.Spell
  ( selectSpellsAndMagicSchoolLevels,
    selectPathsForSpells,
    selectRestrictedNationsForSpells,
    filterSpellByNationId,
  )
where

import Data.Maybe (isJust)
import Data.Text (Text)
import Database.Model.MagicPath
import Database.Model.MagicPathSpell
import Database.Model.MagicSchool
import Database.Model.MagicSchoolSpell
import Database.Model.Nation
import Database.Model.NationSpell
import Database.Model.Spell
import Database.Query.MagicPath
import Database.Query.Utils
import Opaleye

magicSchoolsAndMagicSchoolSpells :: SelectArr () (MagicSchoolField, MagicSchoolSpellField)
magicSchoolsAndMagicSchoolSpells = do
  mss@MagicSchoolSpell
    { magicSchoolId = MagicSchoolId mssMagicSchoolId
    } <-
    selectMagicSchoolSpells
  ms@MagicSchool
    { magicSchoolId = MagicSchoolId magicSchoolId
    } <-
    selectMagicSchools
  viaLateral restrict (magicSchoolId .== mssMagicSchoolId)

  return (ms, mss)

innerJoinMagicSchoolWithSpells ::
  FieldSpellId ->
  Maybe Text ->
  SelectArr () (MaybeFields (MagicSchoolField, MagicSchoolSpellField))
innerJoinMagicSchoolWithSpells spellId schoolM = do
  mssM@( MagicSchool {name},
         MagicSchoolSpell
           { spellId = mssSpellId
           }
         ) <-
    magicSchoolsAndMagicSchoolSpells
  viaLateral restrict (spellId .=== mssSpellId)

  maybeFilterByName name schoolM

  return $ justFields mssM

leftOuterJoinMagicSchoolWithSpells :: FieldSpellId -> SelectArr () (MaybeFields (MagicSchoolField, MagicSchoolSpellField))
leftOuterJoinMagicSchoolWithSpells spellId =
  viaLateral
    (optionalRestrict magicSchoolsAndMagicSchoolSpells)
    $ \( _ms,
         MagicSchoolSpell
           { spellId = mssSpellId
           }
         ) ->
        spellId .=== mssSpellId

selectSpellsAndMagicSchoolLevels :: Maybe NationId -> Maybe Text -> Maybe Text -> Bool -> Select (SpellField, MaybeFields MagicSchoolLevelField)
selectSpellsAndMagicSchoolLevels restrictNationIdM nameM schoolM includeInternal = do
  s@Spell {spellId, name} <- selectSpells
  maybeFilterByNameFuzzy name nameM

  maybe
    (return ())
    (filterSpellByNationId spellId . toFields)
    restrictNationIdM

  mssM <-
    if isJust schoolM || Prelude.not includeInternal
      then innerJoinMagicSchoolWithSpells spellId schoolM
      else leftOuterJoinMagicSchoolWithSpells spellId

  let mssLevel = fmap (\(ms, MagicSchoolSpell {level}) -> (ms, level)) mssM

  return (s, mssLevel)

selectPathsForSpells :: [SpellId] -> Select (FieldSpellId, MagicPathField, Field SqlInt4)
selectPathsForSpells givenIds =
  let bareIds = fmap (\(SpellId sid) -> toFields sid) givenIds
   in fmap (\(_, b, c, d) -> (b, c, d)) $
        orderBy (asc (\(a, _, _, _) -> a)) $ do
          ( mp,
            MagicPathSpell {spellId = SpellId mpsSpellId, level, sorting}
            ) <-
            selectPathsAndPathSpells

          viaLateral restrict (bareIds `in_` mpsSpellId)

          return (sorting, SpellId mpsSpellId, mp, level)

selectRestrictedNationsForSpells :: [SpellId] -> Select (FieldSpellId, NationField)
selectRestrictedNationsForSpells givenSIds = do
  NationSpell
    { spellId = SpellId nsSpellId,
      nationId = nsNationId
    } <-
    selectNationSpells
  n@Nation {nationId} <- selectNations

  let givenIds = fmap (\(SpellId uid) -> toFields uid) givenSIds
  viaLateral restrict (givenIds `in_` nsSpellId)
  viaLateral restrict (nsNationId .=== nationId)

  return (SpellId nsSpellId, n)

filterSpellByNationId :: FieldSpellId -> FieldNationId -> SelectArr () ()
filterSpellByNationId spellId nationId = do
  NationSpell
    { spellId = nsSpellId,
      nationId = nsNationId
    } <-
    selectNationSpells

  viaLateral restrict (nsNationId .=== nationId)
  viaLateral restrict (spellId .=== nsSpellId)

selectPathsAndPathSpells :: Select (MagicPathField, MagicPathSpellField)
selectPathsAndPathSpells = do
  mps@MagicPathSpell
    { magicPathId = mpsMagicPathId
    } <-
    selectMagicPathSpells
  mp@MagicPath {_magicPathId} <- selectMagicPaths

  viaLateral restrict (mpsMagicPathId .=== _magicPathId)

  return (mp, mps)
