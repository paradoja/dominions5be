module Database.Mutations.Nation where

import Data.Int (Int64)
import Database.Model.Nation
  ( Nation,
    nationsTable,
  )
import Opaleye
  ( Delete (..),
    Insert (..),
    iOnConflict,
    rCount,
  )
import Opaleye.ToFields (toFields)

insert :: [Nation] -> Insert Int64
insert ns =
  Insert
    { iTable = nationsTable,
      iRows = map toFields ns,
      iReturning = rCount,
      iOnConflict = Nothing
    }

deleteAll :: Delete Int64
deleteAll =
  Delete
    { dTable = nationsTable,
      dWhere = const $ toFields True,
      dReturning = rCount
    }
