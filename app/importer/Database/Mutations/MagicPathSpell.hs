module Database.Mutations.MagicPathSpell where

import Data.Int (Int64)
import Database.Model.MagicPathSpell
  ( MagicPathSpell,
    magicPathSpellTable,
  )
import Opaleye
  ( Delete (..),
    Insert (..),
    iOnConflict,
    rCount,
  )
import Opaleye.ToFields (toFields)

insert :: [MagicPathSpell] -> Insert Int64
insert mpss =
  Insert
    { iTable = magicPathSpellTable,
      iRows = map toFields mpss,
      iReturning = rCount,
      iOnConflict = Nothing
    }

deleteAll :: Delete Int64
deleteAll =
  Delete
    { dTable = magicPathSpellTable,
      dWhere = const $ toFields True,
      dReturning = rCount
    }
