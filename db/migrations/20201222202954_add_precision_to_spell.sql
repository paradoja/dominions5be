-- migrate:up
ALTER TABLE spells ADD precision smallint NOT NULL DEFAULT 0;

-- migrate:down

ALTER TABLE spells DROP precision;
