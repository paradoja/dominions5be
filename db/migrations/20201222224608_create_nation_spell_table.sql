-- migrate:up
CREATE TABLE nation_spell (
       spell_id integer
         NOT NULL
         REFERENCES spells
         ON DELETE CASCADE,
       nation_id integer
         NOT NULL
         REFERENCES nations
         ON DELETE CASCADE,

       PRIMARY KEY (spell_id, nation_id)
);


-- migrate:down
DROP TABLE nation_spell;
