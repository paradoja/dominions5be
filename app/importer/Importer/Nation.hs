{-# OPTIONS_GHC -fno-warn-orphans #-}

module Importer.Nation where

import Data.Csv
import Database.Model.Era (Era (..))
import Database.Model.Nation (Nation, NationIdT (..), NationT (..))

instance FromNamedRecord Nation where
  parseNamedRecord r =
    Nation
      <$> fmap NationId (r .: "id")
      <*> r .: "name"
      <*> r .: "epithet"
      <*> r .: "abbreviation"
      <*> fmap toEra (r .: "era")
    where
      toEra :: String -> Era
      toEra "1" = Early
      toEra "2" = Middle
      toEra "3" = Late
      toEra e = error $ "Tried to read era " ++ e
