module Database.Model.MagicSchoolSpell where

import Data.Int (Int32)
import Data.Profunctor.Product.TH (makeAdaptorAndInstance)
import Database.Model.MagicSchool
import Database.Model.Spell
import Opaleye

data MagicSchoolSpellT sId msId smallint = MagicSchoolSpell
  { spellId :: sId,
    magicSchoolId :: msId,
    level :: smallint
  }
  deriving (Show)

type MagicSchoolSpell =
  MagicSchoolSpellT SpellId MagicSchoolId Int32

$(makeAdaptorAndInstance "pMagicSchoolSpell" ''MagicSchoolSpellT)

type MagicSchoolSpellField =
  MagicSchoolSpellT
    FieldSpellId
    FieldMagicSchoolId
    (Field SqlInt4)

magicSchoolSpellTable :: Table MagicSchoolSpellField MagicSchoolSpellField
magicSchoolSpellTable =
  table
    "magic_school_spell"
    ( pMagicSchoolSpell
        MagicSchoolSpell
          { spellId = pSpellId (SpellId (tableField "spell_id")),
            magicSchoolId = pMagicSchoolId (MagicSchoolId (tableField "magic_school_id")),
            level = tableField "level"
          }
    )

selectMagicSchoolSpells :: Select MagicSchoolSpellField
selectMagicSchoolSpells = selectTable magicSchoolSpellTable
