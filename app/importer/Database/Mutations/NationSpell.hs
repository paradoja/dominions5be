module Database.Mutations.NationSpell where

import Data.Int (Int64)
import Database.Model.NationSpell
  ( NationSpell,
    nationSpellTable,
  )
import Opaleye
  ( Delete (..),
    Insert (..),
    iOnConflict,
    rCount,
  )
import Opaleye.ToFields (toFields)

insert :: [NationSpell] -> Insert Int64
insert mpss =
  Insert
    { iTable = nationSpellTable,
      iRows = map toFields mpss,
      iReturning = rCount,
      iOnConflict = Nothing
    }

deleteAll :: Delete Int64
deleteAll =
  Delete
    { dTable = nationSpellTable,
      dWhere = const $ toFields True,
      dReturning = rCount
    }
