-- migrate:up
CREATE TABLE spells (
    id integer PRIMARY KEY,
    name character varying NOT NULL,
    fatigue_cost smallint NOT NULL,
    gem_cost smallint NOT NULL,
    damage bigint NOT NULL,
    description text,
    details text
);

CREATE TABLE magic_school_spell (
       spell_id integer
         PRIMARY KEY
         REFERENCES spells
         ON DELETE CASCADE,
       magic_school_id integer
         NOT NULL
         REFERENCES magic_schools
         ON DELETE CASCADE,
       level smallint NOT NULL
);

CREATE TABLE magic_path_spell (
       spell_id integer
         NOT NULL
         REFERENCES spells
         ON DELETE CASCADE,
       magic_path_id integer
         NOT NULL
         REFERENCES magic_paths
         ON DELETE CASCADE,
       sorting smallint NOT NULL,
       level smallint NOT NULL,

       PRIMARY KEY (spell_id, magic_path_id),
       UNIQUE (spell_id, sorting)
);

-- migrate:down
DROP TABLE magic_path_spell;
DROP TABLE magic_school_spell;
DROP TABLE spells;
