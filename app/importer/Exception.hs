module Exception where

import Control.Exception (Exception)

data ImporterException = DecodeException {file :: String, error :: String}
  deriving (Show)

instance Exception ImporterException
