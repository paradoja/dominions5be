# Units

## Plans for units

Units have lots of attributes, variants (not just Unit, Commander,
Pretender, but also other cross-type ones too [like dragon,
undead...]) and other complexities. Some of these are dealt by the
Inspector, but others not.

Eg. some units have different innate forms, for different reasons,
but for example a form when healthy, another when hurt, and another
when about to die. These are listed separately in the
Inspector. This API should link them all and ideally return them
together.

A possible option would be the following, using 1079 as an example.
1079 has a Dying Shape `secondtmpshape` of 1080. 1080 is how it
shows itself sometimes. 1080, thus, has a Natural Shape `first shape` of 1079.

Therefore, with this:

- when requesting all units (or a part of them that includes both),
  1079 should be returned, with 1080 as an alternate form (inside
  the response for 1079),

```json
  {
      "data": {
          "units": [
              ...,
              {
                  "id": "1079",
                  "name": "Chariot Archer",
                  "restrictedToNations": [
                      {
                          "name": "Arcoscephale",
                          "era": EARLY
                      }
                  ],
                  "alternateForms": [
                      "dyingShape": {
                          "id": "1080",
                          "name": "Archerless Chariot"
                      }
                  ]
              }
          ]
      }
  }
```

- when requesting 1079 the same should happen,
- when requesting 1080 (by ID, directly), we should return 1080 with
  a reference to 1079 as a second shape [fn:: Here we have to be wary on the
  server side to avoid cyclic issues, so maybe we should in fact
  return 1079 with 1080 as the `dyingShape` still; the example shows
  the initial proposal, though]

```
  {
      "data": {
          "units": [
              ...,
              {
                  "id": "1080",
                  "name": "Archerless Chariot"
                  "restrictedToNations": [
                      {
                          "name": "Arcoscephale",
                          "era": EARLY
                      }
                  ],
                  "alternateForms": [
                      "firstShape": {
                          "id": "1079",
                          "name": "Chariot Archer"
                      }
                  ]
              }
          ]
      }
  }
```

## Currently supported unit attributes

### Basic attributes

- id
- name
- hitPoints
- size
- strength
- attack
- defense
- protection
- morale
- magicResistance
- precision
- encumbrance
- mapMove
- combatSpeed
- attributes
- description

- starting age is imported somehow, but requires some processing, and is thus not exposed by the API

### Commanders

- for now, units are considered commanders iff they have any magic path levels
- if a unit has any magic capabilities, we return the Magic Levels for each non-zero path

### Other attributes

- undead
- inanimate
- demon
- sailing (sailing ship size, sailing max unit size)

## Fields in file

|     | NAME                                             | DESC                                                            |
| --- | ------------------------------------------------ | --------------------------------------------------------------- |
| 1   | id                                               | id                                                              |
| 2   | name                                             | name                                                            |
| 3   | wpn1                                             | weapon ids                                                      |
| 4   | wpn2                                             |                                                                 |
| 5   | wpn3                                             |                                                                 |
| 6   | wpn4                                             |                                                                 |
| 7   | wpn5                                             |                                                                 |
| 8   | wpn6                                             |                                                                 |
| 9   | wpn7                                             |                                                                 |
| 10  | armor1                                           | armour ids                                                      |
| 11  | armor2                                           |                                                                 |
| 12  | armor3                                           |                                                                 |
| 13  | armor4                                           |                                                                 |
| 14  | rt                                               | recruitment turns                                               |
| 15  | reclimit                                         | recruitment limit                                               |
| 16  | basecost                                         | base cost (if >9000 it is more complex)\*                       |
| 17  | rcost                                            | recruitment cost (with equipment)                               |
| 18  | size                                             | size                                                            |
| 19  | ressize                                          | size modifier for equipment costs                               |
| 20  | hp                                               |                                                                 |
| 21  | prot                                             |                                                                 |
| 22  | mr                                               | base magic resistance                                           |
| 23  | mor                                              | base morale                                                     |
| 24  | str                                              |                                                                 |
| 25  | att                                              |                                                                 |
| 26  | def                                              |                                                                 |
| 27  | prec                                             |                                                                 |
| 28  | enc                                              | encumbrance                                                     |
| 29  | mapmove                                          | map movement points                                             |
| 30  | ap                                               | combat action points                                            |
| 31  | ambidextrous                                     | ambidextrous                                                    |
| 32  | mounted                                          | whether it is mounted or not                                    |
| 33  | reinvigoration                                   |                                                                 |
| 34  | leader                                           |                                                                 |
| 35  | undeadleader                                     |                                                                 |
| 36  | magicleader                                      |                                                                 |
| 37  | startage                                         | starting age                                                    |
| 38  | maxage                                           | max age                                                         |
| 39  | hand                                             | item slot hand                                                  |
| 40  | head                                             |                                                                 |
| 41  | body                                             |                                                                 |
| 42  | foot                                             |                                                                 |
| 43  | misc                                             |                                                                 |
| 44  | crownonly                                        | are head slots just for crowns and similar                      |
| 45  | pathcost                                         | cost new path (gods)                                            |
| 46  | startdom                                         | starting dom. (gods)                                            |
| 47  | inn                                              | innate caster                                                   |
| 48  | F                                                |                                                                 |
| 49  | A                                                |                                                                 |
| 50  | W                                                |                                                                 |
| 51  | E                                                |                                                                 |
| 52  | S                                                |                                                                 |
| 53  | D                                                |                                                                 |
| 54  | N                                                |                                                                 |
| 55  | B                                                |                                                                 |
| 56  | H                                                |                                                                 |
| 57  | rand1                                            | % to get new magic path                                         |
| 58  | nbr1                                             | value of the magic bonus                                        |
| 59  | link1                                            | magic mask (what paths can it be)                               |
| 60  | mask1                                            |                                                                 |
| 61  | rand2                                            |                                                                 |
| 62  | nbr2                                             |                                                                 |
| 63  | link2                                            |                                                                 |
| 64  | mask2                                            |                                                                 |
| 65  | rand3                                            |                                                                 |
| 66  | nbr3                                             |                                                                 |
| 67  | link3                                            |                                                                 |
| 68  | mask3                                            |                                                                 |
| 69  | rand4                                            |                                                                 |
| 70  | nbr4                                             |                                                                 |
| 71  | link4                                            |                                                                 |
| 72  | mask4                                            |                                                                 |
| 73  | holy                                             | sacred                                                          |
| 74  | inquisitor                                       | inquisitor                                                      |
| 75  | mind                                             | mindless                                                        |
| 76  | inanimate                                        | inanimate                                                       |
| 77  | undead                                           |                                                                 |
| 78  | demon                                            |                                                                 |
| 79  | magicbeing                                       |                                                                 |
| 80  | stonebeing                                       |                                                                 |
| 81  | animal                                           |                                                                 |
| 82  | coldblood                                        |                                                                 |
| 83  | female                                           |                                                                 |
| 84  | forestsurvival                                   |                                                                 |
| 85  | mountainsurvival                                 |                                                                 |
| 86  | wastesurvival                                    |                                                                 |
| 87  | swampsurvival                                    |                                                                 |
| 88  | cavesurvival                                     |                                                                 |
| 89  | aquatic                                          |                                                                 |
| 90  | amphibian                                        |                                                                 |
| 91  | pooramphibian                                    |                                                                 |
| 92  | float                                            |                                                                 |
| 93  | flying                                           |                                                                 |
| 94  | stormimmune                                      |                                                                 |
| 95  | teleport                                         |                                                                 |
| 96  | immobile                                         |                                                                 |
| 97  | noriverpass                                      | can't cross rivers                                              |
| 98  | sailingshipsize                                  |                                                                 |
| 99  | sailingmaxunitsize                               |                                                                 |
| 100 | stealthy                                         |                                                                 |
| 101 | illusion                                         |                                                                 |
| 102 | spy                                              |                                                                 |
| 103 | assassin                                         |                                                                 |
| 104 | patience                                         |                                                                 |
| 105 | seduce                                           |                                                                 |
| 106 | succubus                                         | succubus/sleep seduce                                           |
| 107 | corrupt                                          | shaytan seduce                                                  |
| 108 | heal                                             |                                                                 |
| 109 | immortal                                         |                                                                 |
| 110 | domimmortal                                      |                                                                 |
| 111 | reinc                                            | reincarnation? (no one has it)                                  |
| 112 | noheal                                           | unable to heal                                                  |
| 113 | neednoteat                                       | doesn't need to eat                                             |
| 114 | homesick                                         |                                                                 |
| 115 | undisciplined                                    |                                                                 |
| 116 | formationfighter                                 |                                                                 |
| 117 | slave                                            |                                                                 |
| 118 | standard                                         |                                                                 |
| 119 | inspirational                                    |                                                                 |
| 120 | taskmaster                                       | inspirationa for slaves                                         |
| 121 | beastmaster                                      | inspirational for beasts                                        |
| 122 | bodyguard                                        |                                                                 |
| 123 | waterbreathing                                   |                                                                 |
| 124 | iceprot                                          |                                                                 |
| 125 | invulnerable                                     |                                                                 |
| 126 | slashres                                         |                                                                 |
| 127 | bluntres                                         |                                                                 |
| 128 | pierceres                                        |                                                                 |
| 129 | shockres                                         |                                                                 |
| 130 | fireres                                          |                                                                 |
| 131 | coldres                                          |                                                                 |
| 132 | poisonres                                        |                                                                 |
| 133 | voidsanity                                       | void sanity resistance                                          |
| 134 | darkvision                                       |                                                                 |
| 135 | blind                                            |                                                                 |
| 136 | animalawe                                        | animal awe                                                      |
| 137 | awe                                              | awe                                                             |
| 138 | halt                                             | halt heretics                                                   |
| 139 | fear                                             |                                                                 |
| 140 | berserk                                          |                                                                 |
| 141 | cold                                             |                                                                 |
| 142 | heat                                             |                                                                 |
| 143 | fireshield                                       |                                                                 |
| 144 | banefireshield                                   |                                                                 |
| 145 | damagerev                                        | damage reversal                                                 |
| 146 | poisoncloud                                      | poison cloud                                                    |
| 147 | diseasecloud                                     |                                                                 |
| 148 | slimer                                           |                                                                 |
| 149 | mindslime                                        |                                                                 |
| 150 | reform                                           | no one has this                                                 |
| 151 | regeneration                                     | %                                                               |
| 152 | reanimator                                       | # bodies                                                        |
| 153 | barbs                                            |                                                                 |
| 154 | petrify                                          |                                                                 |
| 155 | eyeloss                                          |                                                                 |
| 156 | ethereal                                         |                                                                 |
| 157 | ethtrue                                          | true ehtereal (ignores magic weapons)                           |
| 158 | deathcurse                                       | death curse on death                                            |
| 159 | trample                                          |                                                                 |
| 160 | trampswallow                                     | trample + swallow                                               |
| 161 | stormpower                                       |                                                                 |
| 162 | firepower                                        |                                                                 |
| 163 | coldpower                                        |                                                                 |
| 164 | darkpower                                        |                                                                 |
| 165 | chaospower                                       |                                                                 |
| 166 | magicpower                                       |                                                                 |
| 167 | winterpower                                      |                                                                 |
| 168 | springpower                                      |                                                                 |
| 169 | summerpower                                      |                                                                 |
| 170 | fallpower                                        |                                                                 |
| 171 | forgebonus                                       | discount forging %                                              |
| 172 | fixforgebonus                                    | fixed discount forging                                          |
| 173 | mastersmith                                      | master forger                                                   |
| 174 | resources                                        | resource bonus                                                  |
| 175 | autohealer                                       | healer affliction                                               |
| 176 | autodishealer                                    | healer disease                                                  |
| 177 | alch                                             | empty                                                           |
| 178 | nobadevents                                      | fortune teller                                                  |
| 179 | event                                            | empty                                                           |
| 180 | insane                                           | % insanity                                                      |
| 181 | shatteredsoul                                    | tartarian madness, shattered soul                               |
| 182 | leper                                            | %                                                               |
| 183 | taxcollector                                     |                                                                 |
| 184 | gem                                              | empty                                                           |
| 185 | gemprod                                          | number & type                                                   |
| 186 | chaosrec                                         | chaos recruitment                                               |
| 187 | pillagebonus                                     |                                                                 |
| 188 | patrolbonus                                      |                                                                 |
| 189 | castledef                                        | castle defense bonus                                            |
| 190 | siegebonus                                       |                                                                 |
| 191 | incprovdef                                       | province defense bonus                                          |
| 192 | supplybonus                                      |                                                                 |
| 193 | incunrest                                        | increases unrest                                                |
| 194 | popkill                                          | population kill                                                 |
| 195 | researchbonus                                    |                                                                 |
| 196 | drainimmune                                      | inmune to drain                                                 |
| 197 | inspiringres                                     | inspiring research                                              |
| 198 | douse                                            | slave search bonus                                              |
| 199 | sacr                                             | blood sacrifise bonus                                           |
| 200 | crossbreeder                                     | crossbreeding bonus                                             |
| 201 | makepearls                                       | makes pearls from water gems                                    |
| 202 | pathboost                                        | extra bonus to boosts                                           |
| 203 | allrange                                         | additional magic range                                          |
| 204 | voidsum                                          | void invocation bonus                                           |
| 205 | heretic                                          | heretic                                                         |
| 206 | elegist                                          | elegist (bonus to call god)                                     |
| 207 | shapechange                                      | shapechange (ID of monster)                                     |
| 208 | firstshape                                       | monster ID                                                      |
| 209 | secondshape                                      |                                                                 |
| 210 | secondtmpshape                                   | temporary form when dead                                        |
| 211 | landshape                                        |                                                                 |
| 212 | watershape                                       |                                                                 |
| 213 | forestshape                                      |                                                                 |
| 214 | plainshape                                       |                                                                 |
| 215 | xpshape                                          | xp value to transform                                           |
| 216 | unique                                           |                                                                 |
| 217 | fixedname                                        |                                                                 |
| 218 | special                                          | empty                                                           |
| 219 | nametype                                         | nametype for the unit                                           |
| 220 | summon                                           | ally summon                                                     |
| 221 | n_summon                                         | #                                                               |
| 222 | autosum                                          | id of unit autmatically                                         |
| 223 | n_autosum                                        | #                                                               |
| 224 | batstartsum1                                     | summons at at start of battle                                   |
| 225 | batstartsum2                                     |                                                                 |
| 226 | domsummon                                        | id summoned in dominion                                         |
| 227 | domsummon20                                      | 20%? chance                                                     |
| 228 | raredomsummon                                    | chance8%                                                        |
| 229 | bloodvengeance                                   | blood vengeance                                                 |
| 230 | bringeroffortune                                 | bringer of fortune                                              |
| 231 | realm1                                           | realm                                                           |
| 232 | realm2                                           |                                                                 |
| 233 | realm3                                           |                                                                 |
| 234 | batstartsum3                                     |                                                                 |
| 235 | batstartsum4                                     |                                                                 |
| 236 | batstartsum5                                     |                                                                 |
| 237 | batstartsum1d6                                   | 1d6 summoned at start                                           |
| 238 | batstartsum2d6                                   |                                                                 |
| 239 | batstartsum3d6                                   |                                                                 |
| 240 | batstartsum4d6                                   |                                                                 |
| 241 | batstartsum5d6                                   |                                                                 |
| 242 | batstartsum6d6                                   |                                                                 |
| 243 | turmoilsummon                                    | summon in turmoil provinces                                     |
| 244 | coldsummon                                       | summon in cold provinces                                        |
| 245 | scalewalls                                       |                                                                 |
| 246 | explodeondeath                                   |                                                                 |
| 247 | startaff                                         | starting affliction %                                           |
| 248 | uwregen                                          | underwater regeneration                                         |
| 249 | shrinkhp                                         | shrinking HP                                                    |
| 250 | growhp                                           | growing HP                                                      |
| 251 | transformation                                   | whether it's a transformation (1 good, -1 bad)                  |
| 252 | startingaff                                      | always starts with affliction                                   |
| 253 | fixedresearch                                    | fixed research without magic                                    |
| 254 | divineins                                        | limit of researchers / candle                                   |
| 255 | lamiabonus                                       | lamia summon bonus                                              |
| 256 | preanimator                                      | reanimation bonus by priest                                     |
| 257 | dreanimator                                      | reanimation bonus by dread spell?                               |
| 258 | mummify                                          | unit mummified on death                                         |
| 259 | onebattlespell                                   | auto cast on beginning of battle                                |
| 260 | fireattuned                                      | % bonus on transform (transfor, twiceborn...)                   |
| 261 | airattuned                                       |                                                                 |
| 262 | waterattuned                                     |                                                                 |
| 263 | earthattuned                                     |                                                                 |
| 264 | astralattuned                                    |                                                                 |
| 265 | deathattuned                                     |                                                                 |
| 266 | natureattuned                                    |                                                                 |
| 267 | bloodattuned                                     |                                                                 |
| 268 | magicboostF                                      | bonus to paths on transform                                     |
| 269 | magicboostA                                      |                                                                 |
| 270 | magicboostW                                      |                                                                 |
| 271 | magicboostE                                      |                                                                 |
| 272 | magicboostS                                      |                                                                 |
| 273 | magicboostD                                      |                                                                 |
| 274 | magicboostN                                      |                                                                 |
| 275 | magicboostALL                                    | to all the paths                                                |
| 276 | eyes                                             | #                                                               |
| 277 | heatrec                                          | heat scale requirement to recruit                               |
| 278 | coldrec                                          | cold scale requirement to recruit                               |
| 279 | spreadchaos                                      | spreads chaos                                                   |
| 280 | spreaddeath                                      | spreads death                                                   |
| 281 | corpseeater                                      | eater of death (to get HP)                                      |
| 282 | poisonskin                                       | poisoned skin                                                   |
| 283 | bug                                              | invoked by swarm                                                |
| 284 | uwbug                                            | underwoter bug (invoced by uw swarm)                            |
| 285 | spreadorder                                      | spreads order                                                   |
| 286 | spreadgrowth                                     |                                                                 |
| 287 | startitem                                        | starts with this item, ID                                       |
| 288 | spreaddom                                        |                                                                 |
| 289 | battlesum5                                       | summons 5 of these per battle                                   |
| 290 | acidsplash                                       | splashes acid                                                   |
| 291 | drake                                            | dragon (affected by dragon maser)                               |
| 292 | prophetshape                                     | turns to this when prophet, transform                           |
| 293 | horror                                           | horror type (1 normal, 2 greater, 3 doom horror)                |
| 294 | enchrebate50                                     | 50 gold discount/rebate when this spell is active               |
| 295 | heroarrivallimit                                 | minimum turn to arrive hero                                     |
| 296 | sailsize                                         | size when sailing                                               |
| 297 | uwdamage                                         | % damage uw                                                     |
| 298 | landdamage                                       | % damage on land                                                |
| 299 | rpcost                                           | recruitment points cost                                         |
| 300 | buffer                                           | empty                                                           |
| 301 | rand5                                            | % magic path                                                    |
| 302 | nbr5                                             | how much                                                        |
| 303 | link5                                            | ?                                                               |
| 304 | mask5                                            | paths mask                                                      |
| 305 | rand6                                            |                                                                 |
| 306 | nbr6                                             |                                                                 |
| 307 | link6                                            |                                                                 |
| 308 | mask6                                            |                                                                 |
| 309 | mummification                                    | unit next to which it is mummified with                         |
| 310 | disres                                           | disease resistance %                                            |
| 311 | raiseonkill                                      | raise as soulless on kill %                                     |
| 312 | raiseshape                                       | id unit raised                                                  |
| 313 | sendlesserhorrormult                             | send lesser horror multiplier bonus                             |
| 314 | xploss                                           | xp loss when change of form                                     |
| 315 | theftofthesunawe                                 | awe while theft of the sun is active                            |
| 316 | incorporate                                      | incorporate while swallowed                                     |
| 317 | hpoverslow                                       | HP over maximum that the monster can receive %                  |
| 318 | berserkwhenblessed                               |                                                                 |
| 319 | dragonlord                                       |                                                                 |
| 320 | curseattacker                                    | curses attacker                                                 |
| 321 | uwheataura                                       |                                                                 |
| 322 | slothresearch                                    | bonus from sloth to research (philosopher)                      |
| 323 | horrorsonly                                      | it's a horror                                                   |
| 324 | mindvessel                                       | a possible vessel for an aboleth                                |
| 325 | elementrange                                     | extra range for elemental spells                                |
| 326 | sorceryrange                                     | extra range for sorcery spells                                  |
| 327 | startagemodifier                                 | change to initial age when on this form                         |
| 328 | disbelieve                                       | % break illsusions                                              |
| 329 | firerange                                        |                                                                 |
| 330 | astralrange                                      |                                                                 |
| 331 | landreinvigoration                               | extra reinvigoration on land                                    |
| 332 | naturerange                                      |                                                                 |
| 333 | beartattoo                                       | has a bear tatto (value)                                        |
| 334 | horsetattoo                                      |                                                                 |
| 335 | reincarnation                                    | reborn % on death                                               |
| 336 | wolftattoo                                       |                                                                 |
| 337 | boartattoo                                       |                                                                 |
| 338 | sleepaura                                        |                                                                 |
| 339 | snaketattoo                                      |                                                                 |
| 340 | supplysize                                       | size for supply calc                                            |
| 341 | astralfetters                                    | summons astral fetters on combat                                |
| 342 | foreignmagicboost                                | bonus/malus to magic on foreign provinces (not the initial one) |
| 343 | templetrainer                                    |                                                                 |
| 344 | infernoret                                       | return to inferno possibility per turn                          |
| 345 | kokytosret                                       | return to kokytos possibility per turn                          |
| 346 | infernalcrossbreedingmult                        | infernal cross breeding multiplier                              |
| 347 | unsurr                                           | unsurroundable                                                  |
| 348 | combatcaster                                     | combat caster                                                   |
| 349 | homeshape                                        | shape at home                                                   |
| 350 | speciallook                                      | special sprite 1: Fire Shield, 2: AstralShield, 3: Burning      |
| 351 | aisinglerec                                      | AI recruits just one per batch                                  |
| 352 | nowish                                           | can't be wished in                                              |
| 353 | swarmbody                                        | on death transforms to swarm and reforms                        |
| 354 | mason                                            | architect                                                       |
| 355 | onisummoner                                      | summons Ko-Oni                                                  |
| 356 | sunawe                                           | Sun Awe (only works with sun)                                   |
| 357 | spiritsight                                      |                                                                 |
| 358 | defenceorganiser                                 | bonus to PD (similar to a previous one?)                        |
| 359 | invisibility                                     |                                                                 |
| 360 | startaffliction                                  | possibility to start with affliction (repeated?)                |
| 361 | ivylord                                          | ivy lord                                                        |
| 362 | spellsinger                                      |                                                                 |
| 363 | magicallyattunedresearcher                       | bonus to research / magic scale                                 |
| 364 | triplegod                                        |                                                                 |
| 365 | triplegodmag                                     | malus to magic when the triple god is separate                  |
| 366 | unify                                            | can unify the trinity                                           |
| 367 | triple3mon                                       | trinity is 3 diff. monster                                      |
| 368 | poweroftheturningyear                            |                                                                 |
| 369 | fortkill                                         | % destroy fortress / turn                                       |
| 370 | thronekill                                       | % destroy throne / turn                                         |
| 371 | digest                                           | damage to swollen units                                         |
| 372 | indepmove                                        | % possibility it moves in the map (for horrors)                 |
| 373 | unteleportable                                   | can't teleport                                                  |
| 374 | reanimpriest                                     | reanimator priest                                               |
| 375 | stunimmunity                                     | immunity to stuns                                               |
| 376 | vineshield                                       | vine shiled                                                     |
| 377 | alchemy                                          |                                                                 |
| 378 | woundfend                                        | resistance to afflictions                                       |
| 379 | leavespostbattleifwoundedorhaskilled             | leaves after battle...                                          |
| 380 | falsearmy                                        | shows a smaller army to enemies                                 |
| 381 | summon5                                          | invokes 5 of this automatically                                 |
| 382 | ainorecruit                                      | AI won't recruit this                                           |
| 383 | autocomslave                                     | automatically is a communion slave                              |
| 384 | researchwithoutmagic                             | can do research without paths                                   |
| 385 | slaver                                           | can capture slaves                                              |
| 386 | mustfightinarena                                 |                                                                 |
| 387 | deathwail                                        | wails on death                                                  |
| 388 | adventurers                                      | adventurers                                                     |
| 389 | cleanshape                                       | the monster cleans affliction when changing to first shape      |
| 390 | requireslabtorecruit                             | (... even if not mage)                                          |
| 391 | requirestempletorecruit                          | (... even if not sacred)                                        |
| 392 | horrormarked                                     | horror marked                                                   |
| 393 | changetargetgenderforseductionandseductionimmune | immune to seduction                                             |
| 394 | corpseconstruct                                  |                                                                 |
| 395 | guardianspiritmodifier                           | % probability of being with a guardian spirit in battle         |
| 396 | isashah                                          | is a shah                                                       |
| 397 | iceforging                                       | gives resources based on cold                                   |
| 398 | isayazad                                         | is a yazad                                                      |
| 399 | isadaeva                                         | is a daeva                                                      |
| 400 | flieswhenblessed                                 |                                                                 |
| 401 | plant                                            | is a plant                                                      |
| 402 | clockworklord                                    | clockwork lord                                                  |
| 403 | commaster                                        | automatic communion master                                      |
| 404 | comslave                                         | ???                                                             |
| 405 | minsizeleader                                    | can only be lead by commanders                                  |
| 406 | snowmove                                         | can move in snow                                                |
| 407 | swimming                                         | can swim to cross rivers                                        |
| 408 | stupid                                           | is stupid                                                       |
| 409 | skirmisher                                       | no penalizer for skirmish                                       |
| 410 | ironvul                                          | iron vulnerability                                              |
| 411 | heathensummon                                    | summons heathens                                                |
| 412 | unseen                                           | unseen                                                          |
| 413 | illusionary                                      | is illusion                                                     |
| 414 | immortaltimer                                    | turns to return as immortal                                     |
| 415 | immortalrespawn                                  | ?                                                               |
| 416 | nomovepen                                        | armour movement penalizers ignored on movement                  |
| 417 | wolf                                             | is a wolf                                                       |
| 418 | dungeon                                          | can appear in a dungen                                          |
| 419 | graphicsize                                      | sprite change for the unit                                      |
| 420 | twiceborn                                        | special form on death for the twiceborn                         |
| 421 | aboleth                                          | is aboleth                                                      |
| 422 | tmpastralgems                                    | temporary astral gems for battle                                |
| 423 | sun                                              | unit is a sun/ radiates solar light                             |
| 424 | tmpfiregems                                      | temporary fire gems for battle                                  |
| 425 | defiler                                          | is defiler                                                      |
| 426 | mountedbeserk                                    | mounted berserk                                                 |
| 427 | lanceok                                          | can use lances                                                  |
| 428 | startheroab                                      | % of starting with heroic ability                               |
| 429 | minprison                                        | this god has to start imprisoned                                |
| 430 | uwfireshield                                     |                                                                 |
| 431 | saltvul                                          | salt vulnerability                                              |
| 432 | landenc                                          | additional encumbrance on land                                  |

\* It subtracts 10000 to the value, and then calculates
