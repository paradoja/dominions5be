module Database.Query.Nation
  ( selectNationByIds,
    selectNationsFromEra,
  )
where

import Database.Model
import Opaleye

selectNationByIds :: [NationId] -> Select (FieldNationId, NationField)
selectNationByIds givenIds = do
  n@Nation {nationId = NationId nid} <- selectNations

  let bareIds = fmap (\(NationId bid) -> toFields bid) givenIds
  viaLateral restrict (bareIds `in_` nid)

  pure (NationId nid, n)

selectNationsFromEra :: Maybe Era -> Select NationField
selectNationsFromEra givenEraM = do
  n@Nation {era} <- selectNations
  maybe
    (return ())
    (\givenEra -> viaLateral restrict (era .== toFields givenEra))
    givenEraM

  pure n
