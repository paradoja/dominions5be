{-# LANGUAGE TupleSections #-}

module Importer.Unit (importUnits) where

import Control.Exception (IOException, catch, throw)
import Control.Monad (forM)
import qualified Data.ByteString.Lazy as BL
import Data.Csv
import qualified Data.Map as M
import Data.Maybe (mapMaybe)
import qualified Data.Text.IO as TextIO
import qualified Data.Vector as V
import Database.Model.MagicPath
import Database.Model.MagicPathUnit
import Database.Model.Unit
import qualified Database.Mutations.MagicPathUnit as MMPU
import qualified Database.Mutations.Unit as MU
import Database.PostgreSQL.Simple (Connection)
import Exception (ImporterException (..))
import Importer.Unit.Parsers
import Importer.Utils
import Opaleye (runDelete_, runInsert_)
import System.FilePath ((</>))
import Text.Printf (printf)

importUnits :: Connection -> (FilePath, FilePath) -> [MagicPath] -> IO [Unit]
importUnits c (_, gamedataDir) mps = do
  let file = gamedataDir </> "BaseU.csv"
  let descrPath = gamedataDir </> "unitdescr/"

  csvData <- BL.readFile file

  objects <-
    case decodeByNameWith tsvDecode csvData of
      Left err -> throw $ DecodeException file err
      Right
        (_, v :: V.Vector UnitRowParser) -> return $ V.toList v

  let mpMap =
        M.fromList $
          mapMaybe
            ( \MagicPath {_magicPathId, _abbreviation} ->
                fmap (,_magicPathId) _abbreviation
            )
            mps

  objects <- forM objects $ \((UnitId uid, dlbu), ca, uas, mpidLessMpus) -> do
    let descName = printf "%04d.txt" uid
    desc <-
      (Just <$> TextIO.readFile (descrPath </> descName))
        `catch` (\(_ :: IOException) -> return Nothing)
    let mpus = mapMaybe (\(n, toMPU) -> toMPU <$> M.lookup n mpMap) mpidLessMpus
    return (lowLevelToUnit True (BasicUnit (dlbu desc) (Just ca)) uas [], mpus)

  let (units, mpus) = unzip objects

  let ds = MU.deleteAll
  let is = MU.insert units

  runDelete_ c MMPU.deleteAll

  mapM_ (runDelete_ c) ds
  mapM_ (runInsert_ c) is

  runInsert_ c $ MMPU.insert (concat mpus)

  return units
