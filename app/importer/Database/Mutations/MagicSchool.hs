module Database.Mutations.MagicSchool where

import Data.Int (Int64)
import Database.Model.MagicSchool
  ( MagicSchool,
    magicSchoolsTable,
  )
import Opaleye
  ( Delete (..),
    Insert (..),
    iOnConflict,
    rCount,
  )
import Opaleye.ToFields (toFields)

insert :: [MagicSchool] -> Insert Int64
insert mss =
  Insert
    { iTable = magicSchoolsTable,
      iRows = map toFields mss,
      iReturning = rCount,
      iOnConflict = Nothing
    }

deleteAll :: Delete Int64
deleteAll =
  Delete
    { dTable = magicSchoolsTable,
      dWhere = const $ toFields True,
      dReturning = rCount
    }
