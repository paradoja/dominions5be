module Api (api) where

import Api.Resolvers
import Api.Schema
import Data.ByteString.Lazy.Char8 (ByteString)
import Data.Morpheus (interpreter)
import Data.Morpheus.Types
  ( RootResolver (..),
    Undefined (..),
  )
import Datasource

rootResolver :: RootResolver Haxl () Query Undefined Undefined
rootResolver =
  RootResolver
    { queryResolver =
        Query
          { nations = nationsR,
            nation = nationR,
            magicPaths = magicPathsR,
            spells = spellsR,
            unit = unitR,
            units = unitsR
          },
      mutationResolver = Undefined,
      subscriptionResolver = Undefined
    }

api :: ByteString -> Haxl ByteString
api = interpreter rootResolver
