-- migrate:up
CREATE TYPE era AS ENUM ('early', 'middle', 'late');

CREATE TABLE nations (
    id integer PRIMARY KEY,
    name character varying NOT NULL,
    epithet character varying NOT NULL,
    abbreviation character varying NOT NULL,
    era era NOT NULL
);

-- migrate:down
DROP TABLE nations;
DROP TYPE era;
