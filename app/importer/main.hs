import Control.Exception (throw)
import qualified Data.ByteString.Lazy as BL
import Data.Csv (FromNamedRecord, decodeByNameWith)
import Data.Int (Int64)
import Data.Maybe (fromMaybe)
import qualified Data.Vector as V
import qualified Database.Mutations.MagicPath as MP
import qualified Database.Mutations.MagicSchool as MS
import qualified Database.Mutations.Nation as Nat
import Database.PostgreSQL.Simple (Connection, connectPostgreSQL)
import Database.PostgreSQL.Simple.Transaction (withTransaction)
import Exception
import Importer
import Importer.Utils
import Opaleye
import System.Environment (lookupEnv)
import System.FilePath ((</>))
import Utils (getConnectionString)

defaultResourceDir :: FilePath
defaultResourceDir = "resources/"

gamedataRelativePath :: FilePath
gamedataRelativePath = "dom5inspector/gamedata/"

simpleImportData ::
  FromNamedRecord a =>
  Connection ->
  FilePath ->
  FilePath ->
  Delete Int64 ->
  ([a] -> Insert Int64) ->
  IO ()
simpleImportData c gamedataDir file deleteAll insert = do
  csvData <- BL.readFile $ gamedataDir </> file
  let objects = case decodeByNameWith tsvDecode csvData of
        Left err -> throw $ DecodeException file err
        Right (_, v) -> V.toList v
  _ <- runDelete_ c deleteAll
  _ <- runInsert_ c $ insert objects
  return ()

main :: IO ()
main = do
  connectionString <- getConnectionString
  c <- connectPostgreSQL connectionString

  resourceDirM <- lookupEnv "DOM5_RESOURCE_DIR"
  let resourceDir = fromMaybe defaultResourceDir resourceDirM
  let gamedataDir = resourceDir </> gamedataRelativePath

  _ <- withTransaction c $ do
    simpleImportData c gamedataDir "nations.csv" Nat.deleteAll Nat.insert
    mps <- importMagicPaths c (resourceDir, gamedataDir)
    simpleImportData c gamedataDir "../../constants/magic_schools.csv" MS.deleteAll MS.insert
    _ <- importSpells c (resourceDir, gamedataDir)
    importSpellAttributes c (resourceDir, gamedataDir)
    importUnits c (resourceDir, gamedataDir) mps

  return ()
