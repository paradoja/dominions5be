module Database.Model.Nation where

import Data.Hashable
import Data.Int (Int64)
import Data.Profunctor.Product.TH (makeAdaptorAndInstance)
import Data.Text
import Database.Model.Era
import Opaleye

data NationT nId text era = Nation
  { nationId :: nId,
    name :: text,
    epithet :: text,
    abbreviation :: text,
    era :: era
  }
  deriving (Show)

newtype NationIdT idInt = NationId idInt
  deriving (Show, Eq, Ord)

type NationId = NationIdT Int64

$(makeAdaptorAndInstance "pNationId" ''NationIdT)

instance Hashable NationId where
  hashWithSalt s (NationId a) = hashWithSalt s a

type Nation =
  NationT NationId Text Era

$(makeAdaptorAndInstance "pNation" ''NationT)

type FieldNationId = NationIdT (Field SqlInt8)

type NationField =
  NationT
    FieldNationId
    (Field SqlText)
    (Field SqlEra)

nationsTable :: Table NationField NationField
nationsTable =
  table
    "nations"
    ( pNation
        Nation
          { nationId = pNationId (NationId (tableField "id")),
            name = tableField "name",
            epithet = tableField "epithet",
            abbreviation = tableField "abbreviation",
            era = tableField "era"
          }
    )

selectNations :: Select NationField
selectNations = selectTable nationsTable
