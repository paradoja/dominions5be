module Database.Mutations.MagicSchoolSpell where

import Data.Int (Int64)
import Database.Model.MagicSchoolSpell
  ( MagicSchoolSpell,
    magicSchoolSpellTable,
  )
import Opaleye
  ( Delete (..),
    Insert (..),
    iOnConflict,
    rCount,
  )
import Opaleye.ToFields (toFields)

insert :: [MagicSchoolSpell] -> Insert Int64
insert sps =
  Insert
    { iTable = magicSchoolSpellTable,
      iRows = map toFields sps,
      iReturning = rCount,
      iOnConflict = Nothing
    }

deleteAll :: Delete Int64
deleteAll =
  Delete
    { dTable = magicSchoolSpellTable,
      dWhere = const $ toFields True,
      dReturning = rCount
    }
