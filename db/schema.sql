SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: era; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.era AS ENUM (
    'early',
    'middle',
    'late'
);


--
-- Name: special_ability; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.special_ability AS ENUM (
    'undead',
    'inanimate',
    'demon',
    'sailing'
);


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: magic_path_spell; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.magic_path_spell (
    spell_id integer NOT NULL,
    magic_path_id integer NOT NULL,
    sorting smallint NOT NULL,
    level smallint NOT NULL
);


--
-- Name: magic_path_unit; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.magic_path_unit (
    unit_id integer NOT NULL,
    magic_path_id integer NOT NULL,
    level integer NOT NULL
);


--
-- Name: magic_paths; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.magic_paths (
    id integer NOT NULL,
    name character varying NOT NULL,
    abbreviation character varying
);


--
-- Name: magic_school_spell; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.magic_school_spell (
    spell_id integer NOT NULL,
    magic_school_id integer NOT NULL,
    level smallint NOT NULL
);


--
-- Name: magic_schools; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.magic_schools (
    id integer NOT NULL,
    name character varying NOT NULL
);


--
-- Name: nation_spell; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.nation_spell (
    spell_id integer NOT NULL,
    nation_id integer NOT NULL
);


--
-- Name: nations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.nations (
    id integer NOT NULL,
    name character varying NOT NULL,
    epithet character varying NOT NULL,
    abbreviation character varying NOT NULL,
    era public.era NOT NULL
);


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.schema_migrations (
    version character varying(255) NOT NULL
);


--
-- Name: spells; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.spells (
    id integer NOT NULL,
    name character varying NOT NULL,
    fatigue_cost smallint NOT NULL,
    gem_cost smallint NOT NULL,
    damage bigint NOT NULL,
    description text,
    details text,
    "precision" smallint DEFAULT 0 NOT NULL
);


--
-- Name: unit_leadership_attributes; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.unit_leadership_attributes (
    unit_id integer NOT NULL,
    leadership smallint NOT NULL,
    undead_leadership smallint NOT NULL,
    magical_leadership smallint NOT NULL
);


--
-- Name: unit_special_abilities; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.unit_special_abilities (
    unit_id integer NOT NULL,
    special_ability public.special_ability NOT NULL,
    value1 integer,
    value2 integer
);


--
-- Name: units; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.units (
    id integer NOT NULL,
    name character varying NOT NULL,
    hit_points smallint NOT NULL,
    size smallint NOT NULL,
    strength smallint NOT NULL,
    attack smallint NOT NULL,
    defense smallint NOT NULL,
    protection smallint NOT NULL,
    morale smallint NOT NULL,
    magic_resistance smallint NOT NULL,
    "precision" smallint NOT NULL,
    encumbrance smallint NOT NULL,
    map_move smallint NOT NULL,
    combat_speed smallint NOT NULL,
    starting_age smallint NOT NULL,
    description text
);


--
-- Name: unit_leadership_attributes commander_attributes_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.unit_leadership_attributes
    ADD CONSTRAINT commander_attributes_pkey PRIMARY KEY (unit_id);


--
-- Name: magic_path_spell magic_path_spell_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.magic_path_spell
    ADD CONSTRAINT magic_path_spell_pkey PRIMARY KEY (spell_id, magic_path_id);


--
-- Name: magic_path_spell magic_path_spell_spell_id_sorting_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.magic_path_spell
    ADD CONSTRAINT magic_path_spell_spell_id_sorting_key UNIQUE (spell_id, sorting);


--
-- Name: magic_path_unit magic_path_unit_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.magic_path_unit
    ADD CONSTRAINT magic_path_unit_pkey PRIMARY KEY (unit_id, magic_path_id);


--
-- Name: magic_paths magic_paths_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.magic_paths
    ADD CONSTRAINT magic_paths_pkey PRIMARY KEY (id);


--
-- Name: magic_school_spell magic_school_spell_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.magic_school_spell
    ADD CONSTRAINT magic_school_spell_pkey PRIMARY KEY (spell_id);


--
-- Name: magic_schools magic_schools_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.magic_schools
    ADD CONSTRAINT magic_schools_pkey PRIMARY KEY (id);


--
-- Name: nation_spell nation_spell_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.nation_spell
    ADD CONSTRAINT nation_spell_pkey PRIMARY KEY (spell_id, nation_id);


--
-- Name: nations nations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.nations
    ADD CONSTRAINT nations_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: spells spells_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.spells
    ADD CONSTRAINT spells_pkey PRIMARY KEY (id);


--
-- Name: unit_special_abilities unit_attributes_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.unit_special_abilities
    ADD CONSTRAINT unit_attributes_pkey PRIMARY KEY (unit_id, special_ability);


--
-- Name: units units_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.units
    ADD CONSTRAINT units_pkey PRIMARY KEY (id);


--
-- Name: unit_leadership_attributes commander_attributes_unit_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.unit_leadership_attributes
    ADD CONSTRAINT commander_attributes_unit_id_fkey FOREIGN KEY (unit_id) REFERENCES public.units(id) ON DELETE CASCADE;


--
-- Name: magic_path_spell magic_path_spell_magic_path_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.magic_path_spell
    ADD CONSTRAINT magic_path_spell_magic_path_id_fkey FOREIGN KEY (magic_path_id) REFERENCES public.magic_paths(id) ON DELETE CASCADE;


--
-- Name: magic_path_spell magic_path_spell_spell_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.magic_path_spell
    ADD CONSTRAINT magic_path_spell_spell_id_fkey FOREIGN KEY (spell_id) REFERENCES public.spells(id) ON DELETE CASCADE;


--
-- Name: magic_path_unit magic_path_unit_magic_path_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.magic_path_unit
    ADD CONSTRAINT magic_path_unit_magic_path_id_fkey FOREIGN KEY (magic_path_id) REFERENCES public.magic_paths(id) ON DELETE CASCADE;


--
-- Name: magic_path_unit magic_path_unit_unit_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.magic_path_unit
    ADD CONSTRAINT magic_path_unit_unit_id_fkey FOREIGN KEY (unit_id) REFERENCES public.units(id) ON DELETE CASCADE;


--
-- Name: magic_school_spell magic_school_spell_magic_school_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.magic_school_spell
    ADD CONSTRAINT magic_school_spell_magic_school_id_fkey FOREIGN KEY (magic_school_id) REFERENCES public.magic_schools(id) ON DELETE CASCADE;


--
-- Name: magic_school_spell magic_school_spell_spell_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.magic_school_spell
    ADD CONSTRAINT magic_school_spell_spell_id_fkey FOREIGN KEY (spell_id) REFERENCES public.spells(id) ON DELETE CASCADE;


--
-- Name: nation_spell nation_spell_nation_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.nation_spell
    ADD CONSTRAINT nation_spell_nation_id_fkey FOREIGN KEY (nation_id) REFERENCES public.nations(id) ON DELETE CASCADE;


--
-- Name: nation_spell nation_spell_spell_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.nation_spell
    ADD CONSTRAINT nation_spell_spell_id_fkey FOREIGN KEY (spell_id) REFERENCES public.spells(id) ON DELETE CASCADE;


--
-- Name: unit_special_abilities unit_attributes_unit_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.unit_special_abilities
    ADD CONSTRAINT unit_attributes_unit_id_fkey FOREIGN KEY (unit_id) REFERENCES public.units(id) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--


--
-- Dbmate schema migrations
--

INSERT INTO public.schema_migrations (version) VALUES
    ('20201216192351'),
    ('20201219200214'),
    ('20201219231238'),
    ('20201220171316'),
    ('20201222202954'),
    ('20201222224608'),
    ('20201224113601'),
    ('20201228161822'),
    ('20210103214123'),
    ('20210104160250'),
    ('20210105164258'),
    ('20210105180157');
