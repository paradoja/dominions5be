{-# OPTIONS_GHC -fno-warn-orphans #-}

module Importer.MagicPathUnit where

import Control.Monad (forM)
import qualified Data.ByteString as B
import Data.Csv
import Data.Maybe (catMaybes)
import Data.Text (Text)
import Data.Text.Encoding (decodeUtf8)
import Database.Model.MagicPath
import Database.Model.MagicPathUnit
import Importer.Unit.UnitId

-- we have to get the magic_path_id to build the full mpu
type MPLessMPU = (Text, MagicPathId -> MagicPathUnit)

instance FromNamedRecord [MPLessMPU] where
  parseNamedRecord r = do
    pid <- getId r
    let names = ["F", "A", "W", "E", "S", "D", "N", "B", "H"]
    objects <- forM names $ \n -> do
      mplevelM <- r .: n
      return $ fmap (\mplevel -> (decodeUtf8 n, \mpid -> MagicPathUnit pid mpid mplevel)) mplevelM

    return $ catMaybes objects
