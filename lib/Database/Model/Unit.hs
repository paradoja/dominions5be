{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}

module Database.Model.Unit
  ( module Database.Model.Unit,
    module U,
  )
where

import Control.Lens
import Control.Lens.TH
import Data.Int (Int32, Int64)
import Data.Profunctor.Product.TH (makeAdaptorAndInstanceInferrable)
import Database.Model.MagicPath
import Database.Model.Unit.BasicUnitAttributes as U
import Database.Model.Unit.LeadershipAttributes as U hiding (unitId)
import Database.Model.Unit.SpecialAbility as U
import Database.Model.Unit.UnitSpecialAbility as U hiding (unitId)
import Database.Model.Unit.Utils as U
import Opaleye (MaybeFields)

-- A BasicUnit represents what we can get from the DB in one direct
-- request.
data BasicUnitT basicUnitAttributes maybeLeadershipAttributes = BasicUnit
  { _unitAttributes :: basicUnitAttributes,
    _maybeLeadershipAttributes :: maybeLeadershipAttributes -- all units somehow have leadership attributes
  }
  deriving (Show)

$(makeLenses ''BasicUnitT)
$(makeAdaptorAndInstanceInferrable "pBasicUnit" ''BasicUnitT)

type BasicUnit =
  BasicUnitT
    BasicUnitAttributes
    (Maybe LeadershipAttributes)

type BasicUnitField =
  BasicUnitT
    BasicUnitAttributesField
    (MaybeFields LeadershipAttributesField)

-- TODO - clean . To allow easy importing for now we have an argument
-- to force the unit to be regarded as a commander.
lowLevelToUnit :: Bool -> BasicUnit -> [UnitSpecialAbility] -> [MagicPathLevel] -> Unit
lowLevelToUnit forceCommander basicUnit unitSA magicPathsLevels =
  let basicUnitAttrs = basicUnit ^. unitAttributes
      mla = basicUnit ^. maybeLeadershipAttributes
   in case (forceCommander, mla, magicPathsLevels) of
        (True, Just leadershipAttributes, mps) ->
          Commander
            basicUnitAttrs
            unitSA
            leadershipAttributes
            mps
        (_, _, []) -> BaseUnit basicUnitAttrs unitSA
        (_, Just leadershipAttributes, mps) ->
          Commander
            basicUnitAttrs
            unitSA
            leadershipAttributes
            mps
        _ -> error $ "Impossible unit " ++ show (basicUnit, magicPathsLevels)

-- This is what determines the type of unit we have
toUnit :: BasicUnit ->[UnitSpecialAbility] -> [MagicPathLevel] -> Unit
toUnit = lowLevelToUnit False

data Unit
  = BaseUnit
      { _basicUnitAttributes :: BasicUnitAttributes,
        _specialAbilities :: [UnitSpecialAbility]
      }
  | Commander
      { _basicUnitAttributes :: BasicUnitAttributes,
        _specialAbilities :: [UnitSpecialAbility],
        _leadershipAttributes :: LeadershipAttributes,
        _magicPaths :: [(MagicPath, Int32)]
      }
  deriving (Show)

$(makeLenses ''Unit)
