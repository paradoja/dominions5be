module Database.Query.Unit
  ( selectBasicUnitsForName,
    selectBasicUnitByIds,
    selectUnitSpecialAbilitiesByIds,
    selectUnitMagicPathLevelsByIds,
  )
where

import Control.Lens
import Data.Text (Text)
import Database.Model.MagicPath
import qualified Database.Model.MagicPathUnit as MPU
import Database.Model.Unit
import Database.Query.MagicPath
import Database.Query.Utils
import Opaleye

selectBasicUnitAttributesByIds :: [UnitId] -> Select (FieldUnitId, BasicUnitAttributesField)
selectBasicUnitAttributesByIds givenUIds = do
  let givenIds = fmap (\(UnitId uid) -> toFields uid) givenUIds
  u@BasicUnitAttributes {_unitId = UnitId uid} <- selectBasicUnitAttributes
  viaLateral restrict (givenIds `in_` uid)
  pure (UnitId uid, u)

leftOuterJoinLeadershipAttributes :: FieldUnitId -> SelectArr () (MaybeFields LeadershipAttributesField)
leftOuterJoinLeadershipAttributes givenUnitId =
  viaLateral
    (optionalRestrict selectLeadershipAttributes)
    $ \LeadershipAttributes
         { _unitId
         } ->
        givenUnitId .=== _unitId

selectBasicUnitsForName :: Maybe Text -> Select (FieldNullable SqlText, BasicUnitField)
selectBasicUnitsForName nameM =
  let filterBUByName = do
        bu@BasicUnitAttributes {_name} <- selectBasicUnitAttributes
        maybeFilterByNameFuzzy _name nameM
        return (maybeToNullable (toFields <$> nameM), bu)
   in selectBasicUnitsFromSelector filterBUByName

selectBasicUnitByIds :: [UnitId] -> Select (FieldUnitId, BasicUnitField)
selectBasicUnitByIds = selectBasicUnitsFromSelector . selectBasicUnitAttributesByIds

selectBasicUnitsFromSelector :: Select (a, BasicUnitAttributesField) -> Select (a, BasicUnitField)
selectBasicUnitsFromSelector buSelector = do
  (something, bu@BasicUnitAttributes {_unitId}) <- buSelector
  ca <- leftOuterJoinLeadershipAttributes _unitId

  return (something, BasicUnit bu ca)

selectUnitSpecialAbilitiesByIds :: [UnitId] -> Select (FieldUnitId, UnitSpecialAbilityField)
selectUnitSpecialAbilitiesByIds givenUIds = do
  let givenIds = fmap (\(UnitId uid) -> toFields uid) givenUIds
  ua@UnitSpecialAbility {_unitId = UnitId uid} <- selectUnitSpecialAbilities
  viaLateral restrict (givenIds `in_` uid)
  pure (UnitId uid, ua)

selectUnitMagicPathLevelsByIds :: [UnitId] -> Select (FieldUnitId, MagicPathLevelField)
selectUnitMagicPathLevelsByIds givenUIds = do
  let givenIds = fmap (\(UnitId uid) -> toFields uid) givenUIds

  mpu <- MPU.selectMagicPathUnits
  mp@MagicPath {_magicPathId} <- selectMagicPaths

  let currentUId@(UnitId bareId) = mpu ^. MPU.unitId
  viaLateral restrict (givenIds `in_` bareId)
  viaLateral restrict (mpu ^. MPU.magicPathId .=== _magicPathId)

  return (currentUId, (mp, mpu ^. MPU.level))
