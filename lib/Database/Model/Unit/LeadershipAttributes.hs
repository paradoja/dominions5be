module Database.Model.Unit.LeadershipAttributes where

import Control.Lens.TH
import Data.Int (Int32)
import Data.Profunctor.Product.TH (makeAdaptorAndInstance)
import Database.Model.Unit.Utils
import Opaleye
  ( Field,
    Select,
    SqlInt4,
    Table,
    restrict,
    selectTable,
    table,
    tableField,
    toFields,
    viaLateral,
    (.===),
  )

data LeadershipAttributesT uid smallint = LeadershipAttributes
  { _unitId :: uid,
    _leadership :: smallint,
    _undeadLeadership :: smallint,
    _magicalLeadership :: smallint
  }
  deriving (Show)

$(makeLenses ''LeadershipAttributesT)
$(makeAdaptorAndInstance "pLeadershipAttributes" ''LeadershipAttributesT)

type LeadershipAttributes =
  LeadershipAttributesT UnitId Int32

type LeadershipAttributesField =
  LeadershipAttributesT
    FieldUnitId
    (Field SqlInt4)

leadershipAttributesTable :: Table LeadershipAttributesField LeadershipAttributesField
leadershipAttributesTable =
  table
    "unit_leadership_attributes"
    ( pLeadershipAttributes
        LeadershipAttributes
          { _unitId = pUnitId (UnitId (tableField "unit_id")),
            _leadership = tableField "leadership",
            _undeadLeadership = tableField "undead_leadership",
            _magicalLeadership = tableField "magical_leadership"
          }
    )

selectLeadershipAttributes :: Select LeadershipAttributesField
selectLeadershipAttributes = selectTable leadershipAttributesTable
