module Importer.Unit.UnitId where

import Data.Csv (NamedRecord, Parser, (.:))
import Database.Model.Unit (UnitId, UnitIdT (..))

getId :: NamedRecord -> Parser UnitId
getId r = fmap UnitId (r .: "id")
