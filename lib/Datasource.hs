module Datasource (module Datasource, module DB) where

import Datasource.Database
import Datasource.Database as DB (DBNotFound, State (..))
import Haxl.Core

type Haxl = GenHaxl () (State DBRequest)

run :: Env () (State DBRequest) -> Haxl a -> IO a
run = runHaxl
