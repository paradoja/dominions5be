module Database.Mutations.Unit.BasicUnitAttributes where

import Data.Int (Int64)
import Database.Model.Unit
import Opaleye
  ( Delete (..),
    Insert (..),
    iOnConflict,
    rCount,
  )
import Opaleye.ToFields (toFields)

insert :: [BasicUnitAttributes] -> Insert Int64
insert sps =
  Insert
    { iTable = basicUnitAttributesTable,
      iRows = map toFields sps,
      iReturning = rCount,
      iOnConflict = Nothing
    }

deleteAll :: Delete Int64
deleteAll =
  Delete
    { dTable = basicUnitAttributesTable,
      dWhere = const $ toFields True,
      dReturning = rCount
    }
