-- migrate:up
CREATE TABLE magic_schools (
    id integer PRIMARY KEY,
    name character varying NOT NULL
);

-- migrate:down

DROP TABLE magic_schools;
