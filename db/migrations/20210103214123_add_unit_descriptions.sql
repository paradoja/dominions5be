-- migrate:up
ALTER TABLE units ADD description TEXT;

-- migrate:down

ALTER TABLE units DROP description;
