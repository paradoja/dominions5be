module Database.Mutations.Unit.LeadershipAttributes where

import Data.Int (Int64)
import Database.Model.Unit
import Opaleye
  ( Delete (..),
    Insert (..),
    iOnConflict,
    rCount,
  )
import Opaleye.ToFields (toFields)

insert :: [LeadershipAttributes] -> Insert Int64
insert objs =
  Insert
    { iTable = leadershipAttributesTable,
      iRows = map toFields objs,
      iReturning = rCount,
      iOnConflict = Nothing
    }

deleteAll :: Delete Int64
deleteAll =
  Delete
    { dTable = leadershipAttributesTable,
      dWhere = const $ toFields True,
      dReturning = rCount
    }
