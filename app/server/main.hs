import Api (api)
import Data.Maybe (fromMaybe)
import Data.Pool (createPool)
import Database.PostgreSQL.Simple
import Datasource
import Haxl.Core (initEnv, stateEmpty, stateSet)
import Network.Wai.Middleware.RequestLogger (logStdout)
import System.Environment (lookupEnv)
import Utils (getConnectionString)
import Web.Scotty

defaultPort :: String -- it will be `read`
defaultPort = "3000"

main :: IO ()
main = do
  connectionString <- getConnectionString
  pool <-
    createPool
      (connectPostgreSQL connectionString)
      close
      1 -- stripes (sub-pools)
      60 -- unused connections are kept open for a minute
      10 -- max. 10 connections open per stripe
  let stateStore = stateSet (DBState pool) stateEmpty
  env0 <- initEnv stateStore ()
  portM <- lookupEnv "PORT"
  let port = read $ fromMaybe defaultPort portM
  scotty port $ do
    middleware logStdout
    post "/api" $
      raw =<< (liftAndCatchIO . (run env0 . api) =<< body)
    get "/graphql" $ do
      setHeader "Content-Type" "text/html; charset=utf-8"
      file "resources/graphiql.html"
