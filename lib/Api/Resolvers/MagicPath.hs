module Api.Resolvers.MagicPath (magicPathsR, toGQL, magicPathLevelToGQL) where

import Api.Schema
import Data.Morpheus.Types
import Data.Text (pack)
import Database.Fetch
import qualified Database.Model as M
import Datasource

toGQL :: M.MagicPath -> MagicPath (MQueryF a)
toGQL M.MagicPath {_magicPathId, _name} =
  MagicPath
    { id = pure $ ID (pack $ show _magicPathId),
      name = pure _name
    }

magicPathLevelToGQL :: M.MagicPathLevel -> MagicPathLevel (MQueryF a)
magicPathLevelToGQL (M.MagicPath {_magicPathId = M.MagicPathId magicPathId, _name}, level) =
  MagicPathLevel
    { magicPath =
        pure
          MagicPath
            { id = pure $ ID (pack $ show magicPathId),
              name = pure _name
            },
      level = pure $ fromIntegral level
    }

magicPathsR :: ResolverQ e Haxl [MagicPath (MQueryF ())]
magicPathsR = lift $ fmap toGQL <$> getMagicPaths
