module Database.Mutations.MagicPath where

import Data.Int (Int64)
import Database.Model.MagicPath
  ( MagicPath,
    magicPathsTable,
  )
import Opaleye
  ( Delete (..),
    Insert (..),
    iOnConflict,
    rCount,
  )
import Opaleye.ToFields (toFields)

insert :: [MagicPath] -> Insert Int64
insert mps =
  Insert
    { iTable = magicPathsTable,
      iRows = map toFields mps,
      iReturning = rCount,
      iOnConflict = Nothing
    }

deleteAll :: Delete Int64
deleteAll =
  Delete
    { dTable = magicPathsTable,
      dWhere = const $ toFields True,
      dReturning = rCount
    }
