module Database.Mutations.Spell where

import Data.Int (Int64)
import Database.Model.Spell
  ( Spell,
    spellsTable,
  )
import Opaleye
  ( Delete (..),
    Insert (..),
    iOnConflict,
    rCount,
  )
import Opaleye.ToFields (toFields)

insert :: [Spell] -> Insert Int64
insert sps =
  Insert
    { iTable = spellsTable,
      iRows = map toFields sps,
      iReturning = rCount,
      iOnConflict = Nothing
    }

deleteAll :: Delete Int64
deleteAll =
  Delete
    { dTable = spellsTable,
      dWhere = const $ toFields True,
      dReturning = rCount
    }
