module RawAttribute where

import Data.Text (Text)

data RawAttribute = RawAttribute
  { code :: Int,
    name :: Text,
    key :: Text,
    category :: RawAttributeType,
    comment :: Text
  }

data RawAttributeType
  = SpellAttribute
  | NationAttribute
  | GeneralAttribute -- for known attr. without class
  | UnknownCategoryAttribute

restrictToNation :: RawAttribute
restrictToNation =
  RawAttribute
    278
    "Restrict to Nation"
    "restricted"
    SpellAttribute
    "Nation value is ID + 100"
