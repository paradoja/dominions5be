module Api.Resolvers (module R) where

import Api.Resolvers.MagicPath as R
import Api.Resolvers.Nation as R hiding (toGQL)
import Api.Resolvers.Spell as R
import Api.Resolvers.Unit as R hiding (toGQL)
