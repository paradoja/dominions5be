module Database.Fetch.MagicPath where

import Database.Fetch.Utils
import Database.Model

getMagicPaths :: Haxl [MagicPath]
getMagicPaths = dataFetch FetchMagicPaths
