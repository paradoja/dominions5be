module Database.Model.MagicSchool where

import Data.Int (Int32, Int64)
import Data.Profunctor.Product.TH (makeAdaptorAndInstance)
import Data.Text
import Opaleye

data MagicSchoolT msId text = MagicSchool
  { magicSchoolId :: msId,
    name :: text
  }
  deriving (Show)

newtype MagicSchoolIdT idInt = MagicSchoolId idInt
  deriving (Show)

type MagicSchoolId = MagicSchoolIdT Int64

$(makeAdaptorAndInstance "pMagicSchoolId" ''MagicSchoolIdT)

type MagicSchool =
  MagicSchoolT MagicSchoolId Text

$(makeAdaptorAndInstance "pMagicSchool" ''MagicSchoolT)

type FieldMagicSchoolId = MagicSchoolIdT (Field SqlInt8)

type MagicSchoolField =
  MagicSchoolT
    FieldMagicSchoolId
    (Field SqlText)

type MagicSchoolLevel = (MagicSchool, Int32)

type MagicSchoolLevelField = (MagicSchoolField, Field SqlInt4)

magicSchoolsTable :: Table MagicSchoolField MagicSchoolField
magicSchoolsTable =
  table
    "magic_schools"
    ( pMagicSchool
        MagicSchool
          { magicSchoolId = pMagicSchoolId (MagicSchoolId (tableField "id")),
            name = tableField "name"
          }
    )

selectMagicSchools :: Select MagicSchoolField
selectMagicSchools = selectTable magicSchoolsTable
