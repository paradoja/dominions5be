module Api.Resolvers.Unit (unitsR, unitR, toGQL) where

import Api.Resolvers.MagicPath (magicPathLevelToGQL)
import Api.Resolvers.Utils
import Api.Schema
import Control.Lens
import Data.Morpheus.Types
import Data.Text (pack)
import Database.Fetch
import qualified Database.Model as M
import Database.Model.Unit as M.U hiding (BaseUnit, Commander, SpecialAbility, Unit)
import qualified Database.Model.Unit as M
import Datasource
import Haxl.Core (catch)

unitR :: UnitArgs -> ResolverQ e Haxl Unit
unitR UnitArgs {id = idM} =
  liftEither $
    either
      (return . Left)
      ( \uid ->
          fmap (Right . toGQL) (unitFromBasicUnit =<< getBasicUnitById (UnitId uid))
            `catch` ( \(_ :: DBNotFound) ->
                        return . Left $
                          concat ["Unit with id ", show uid, " not found"]
                    )
      )
      (idFromGraphQL idM)

unitsR :: UnitsArgs -> ResolverQ e Haxl [Unit (MQueryF ())]
unitsR UnitsArgs {name = nameM} =
  lift (getBasicUnitsForName nameM >>= mapM (fmap toGQL . unitFromBasicUnit))

-- unlike other `toGQL` functions, this returns inside the enclosing
-- monad as to allow the `fail` condition.
unitSpecialAbilityToGQL :: M.UnitSpecialAbility -> MQueryF a (SpecialAbility (MQueryF a))
unitSpecialAbilityToGQL (UnitSpecialAbility _ (M.SpecialAbility Sailing _) (Just v1) (Just v2)) =
  pure $
    SpecialAbility
      (pure "Sailing")
      (pure . Just $ "`value1` is sailing ship size, `value2` is sailing max unit size")
      (pure . Just $ fromIntegral v1)
      (pure . Just $ fromIntegral v2)
unitSpecialAbilityToGQL (UnitSpecialAbility _ (M.SpecialAbility a 1) (Just v1) Nothing) =
  pure $
    SpecialAbility
      (pure . pack . show $ a)
      (pure Nothing)
      (pure . Just $ fromIntegral v1)
      (pure Nothing)
unitSpecialAbilityToGQL (UnitSpecialAbility _ (M.SpecialAbility a 0) Nothing Nothing) =
  pure $
    SpecialAbility
      (pure . pack . show $ a)
      (pure Nothing)
      (pure Nothing)
      (pure Nothing)
unitSpecialAbilityToGQL u = fail $ "Invalid attribute " ++ show u

toGQL :: M.Unit -> Unit (MQueryF a)
toGQL unit =
  let UnitId uid = unit ^. M.basicUnitAttributes . unitId
   in case unit of
        -- this code can be deduplicated, but I haven't found a way of
        -- doing it that doesn't rely on order of arguments (and there
        -- are lots) or that looks hypercomplicated
        M.BaseUnit basicAtts unitSpecialAbilities ->
          UnitBaseUnit
            BaseUnit
              { id = pure $ ID (pack $ show uid),
                name = pure $ basicAtts ^. M.U.name,
                hitPoints = pure . fromIntegral $ basicAtts ^. M.hitPoints,
                size = pure . fromIntegral $ basicAtts ^. M.size,
                strength = pure . fromIntegral $ basicAtts ^. M.strength,
                attack = pure . fromIntegral $ basicAtts ^. M.attack,
                defense = pure . fromIntegral $ basicAtts ^. M.defense,
                protection = pure . fromIntegral $ basicAtts ^. M.protection,
                morale = pure . fromIntegral $ basicAtts ^. M.morale,
                magicResistance = pure . fromIntegral $ basicAtts ^. M.magicResistance,
                precision = pure . fromIntegral $ basicAtts ^. M.U.precision,
                encumbrance = pure . fromIntegral $ basicAtts ^. M.encumbrance,
                mapMove = pure . fromIntegral $ basicAtts ^. M.mapMove,
                combatSpeed = pure . fromIntegral $ basicAtts ^. M.combatSpeed,
                specialAbilities = mapM unitSpecialAbilityToGQL unitSpecialAbilities,
                description = pure $ basicAtts ^. M.U.description
              }
        M.Commander basicAtts unitSpecialAbilities leadAtts magicPaths ->
          UnitCommander
            Commander
              { id = pure $ ID (pack $ show uid),
                name = pure $ basicAtts ^. M.U.name,
                hitPoints = pure . fromIntegral $ basicAtts ^. M.hitPoints,
                size = pure . fromIntegral $ basicAtts ^. M.size,
                strength = pure . fromIntegral $ basicAtts ^. M.strength,
                attack = pure . fromIntegral $ basicAtts ^. M.attack,
                defense = pure . fromIntegral $ basicAtts ^. M.defense,
                protection = pure . fromIntegral $ basicAtts ^. M.protection,
                morale = pure . fromIntegral $ basicAtts ^. M.morale,
                magicResistance = pure . fromIntegral $ basicAtts ^. M.magicResistance,
                precision = pure . fromIntegral $ basicAtts ^. M.U.precision,
                encumbrance = pure . fromIntegral $ basicAtts ^. M.encumbrance,
                mapMove = pure . fromIntegral $ basicAtts ^. M.mapMove,
                combatSpeed = pure . fromIntegral $ basicAtts ^. M.combatSpeed,
                specialAbilities = mapM unitSpecialAbilityToGQL unitSpecialAbilities,
                description = pure $ basicAtts ^. M.U.description,
                -- commander attrs
                leadership = pure . fromIntegral $ leadAtts ^. M.leadership,
                undeadLeadership = pure . fromIntegral $ leadAtts ^. M.undeadLeadership,
                magicalLeadership = pure . fromIntegral $ leadAtts ^. M.magicalLeadership,
                magicPaths = pure $ fmap magicPathLevelToGQL magicPaths
              }
