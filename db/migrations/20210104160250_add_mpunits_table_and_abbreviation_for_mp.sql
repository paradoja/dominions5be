-- migrate:up
CREATE TABLE magic_path_unit (
    unit_id integer
      NOT NULL
      REFERENCES units
      ON DELETE CASCADE,
    magic_path_id integer
      NOT NULL
      REFERENCES magic_paths
      ON DELETE CASCADE,
    level integer NOT NULL,

    PRIMARY KEY (unit_id, magic_path_id)
);

ALTER TABLE magic_paths ADD abbreviation character varying;

-- migrate:down

ALTER TABLE magic_paths DROP abbreviation;
DROP TABLE magic_path_unit;
