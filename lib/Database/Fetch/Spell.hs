module Database.Fetch.Spell where

import Control.Monad (forM)
import Data.Int (Int32)
import Data.Text (Text)
import Database.Fetch.Utils
import Database.Model

getSpellsAndMagicSchools ::
  -- | Restrict to this nation's spells
  Maybe NationId ->
  -- | Search by spell name
  Maybe Text ->
  -- | Search by Magic School
  Maybe Text ->
  -- | Include internal spells
  Bool ->
  Haxl [(Spell, Maybe (MagicSchool, Int32))]
getSpellsAndMagicSchools nid sname school internal =
  dataFetch $ FetchSpellsAndMagicSchools nid sname school internal

getMagicPathsForSpell :: SpellId -> Haxl [(MagicPath, Int32)]
getMagicPathsForSpell = dataFetch . FetchMagicPathsForSpell

getRestrictedNationsForSpell :: SpellId -> Haxl [Nation]
getRestrictedNationsForSpell = dataFetch . FetchRestrictedNationsForSpell

getFullSpells ::
  -- | Search only spells restricted to this nation
  Maybe NationId ->
  -- | Search spell name
  Maybe Text ->
  -- | Spells of this Magic School
  Maybe Text ->
  -- | Include "internal" spells
  Bool ->
  Haxl [(Spell, Maybe (MagicSchool, Int32), [(MagicPath, Int32)], [Nation])]
getFullSpells nid sname school internal = do
  list <- getSpellsAndMagicSchools nid sname school internal
  forM list $ \(s@Spell {spellId}, msM) -> do
    mps <- getMagicPathsForSpell spellId
    ns <- getRestrictedNationsForSpell spellId
    return (s, msM, mps, ns)
