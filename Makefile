.PHONY: deploy update

deploy:
	deploy/deploy

update:
	wget -nv https://raw.githubusercontent.com/graphql/graphiql/main/examples/graphiql-cdn/index.html -O resources/graphiql.html
	sed -i 's|https://swapi-.*/index|/api|' resources/graphiql.html
