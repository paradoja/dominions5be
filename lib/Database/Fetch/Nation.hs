module Database.Fetch.Nation where

import Database.Fetch.Utils
import Database.Model

getNationById :: NationId -> Haxl Nation
getNationById = dataFetch . FetchNation

getNationsByEra :: Maybe Era -> Haxl [Nation]
getNationsByEra = dataFetch . FetchNations
