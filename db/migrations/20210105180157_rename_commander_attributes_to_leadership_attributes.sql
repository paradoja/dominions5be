-- migrate:up

ALTER TABLE commander_attributes RENAME TO unit_leadership_attributes;

-- migrate:down

ALTER TABLE unit_leadership_attributes RENAME TO commander_attributes;
