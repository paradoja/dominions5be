{-# OPTIONS_GHC -fno-warn-orphans #-}

module Importer.MagicPath (importMagicPaths) where

import Control.Exception (IOException, throw)
import Control.Monad (forM)
import qualified Data.ByteString.Lazy as BL
import Data.Csv
import qualified Data.Map as M
import Data.Text (Text)
import qualified Data.Vector as V
import Database.Model.MagicPath (MagicPath, MagicPathIdT (..), MagicPathT (..))
import Database.Mutations.MagicPath
import Database.PostgreSQL.Simple (Connection)
import Exception (ImporterException (..))
import Importer.Utils
import Opaleye (runDelete_, runInsert_)
import System.FilePath ((</>))

type AbbrLessMP = (Maybe Text -> MagicPath)

instance FromNamedRecord (Text, Text) where
  parseNamedRecord r = (,) <$> (r .: "name") <*> (r .: "abbreviation")

instance FromNamedRecord (Text, AbbrLessMP) where
  parseNamedRecord r = do
    nameP <- r .: "name"
    almp <-
      MagicPath
        <$> fmap MagicPathId (r .: "number")
        <*> r .: "name"

    return (nameP, almp)

importMagicPaths :: Connection -> (FilePath, FilePath) -> IO [MagicPath]
importMagicPaths c (resourceDir, gamedataDir) = do
  let abbrFile = resourceDir </> "constants" </> "magic_path_abbreviations.csv"
  let file = gamedataDir </> "magic_paths.csv"

  abbrData <- BL.readFile abbrFile
  csvData <- BL.readFile file
  let abbreviations = case decodeByNameWith tsvDecode abbrData of
        Left err -> throw $ DecodeException file err
        Right (_, v :: V.Vector (Text, Text)) -> M.fromList $ V.toList v

  let objects = case decodeByNameWith tsvDecode csvData of
        Left err -> throw $ DecodeException file err
        Right (_, v :: V.Vector (Text, AbbrLessMP)) -> V.toList v

  magicPaths <- forM objects $ \(n, almp) ->
    return $ almp (M.lookup n abbreviations)

  _ <- runDelete_ c deleteAll
  _ <- runInsert_ c $ insert magicPaths

  return magicPaths
