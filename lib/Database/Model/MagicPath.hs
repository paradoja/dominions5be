module Database.Model.MagicPath where

import Control.Lens.TH
import Data.Int (Int32, Int64)
import Data.Profunctor.Product.TH (makeAdaptorAndInstance)
import Data.Text
import Opaleye

data MagicPathT mpid text maybeText = MagicPath
  { _magicPathId :: mpid,
    _name :: text,
    _abbreviation :: maybeText
  }
  deriving (Show)

$(makeLenses ''MagicPathT)
$(makeAdaptorAndInstance "pMagicPath" ''MagicPathT)

newtype MagicPathIdT idInt = MagicPathId idInt
  deriving (Show)

type MagicPathId = MagicPathIdT Int64

$(makeAdaptorAndInstance "pMagicPathId" ''MagicPathIdT)

type MagicPath =
  MagicPathT MagicPathId Text (Maybe Text)

type FieldMagicPathId = MagicPathIdT (Field SqlInt8)

type MagicPathField =
  MagicPathT
    FieldMagicPathId
    (Field SqlText)
    (FieldNullable SqlText)

type MagicPathLevel = (MagicPath, Int32)

type MagicPathLevelField = (MagicPathField, Field SqlInt4)

magicPathsTable :: Table MagicPathField MagicPathField
magicPathsTable =
  table
    "magic_paths"
    ( pMagicPath
        MagicPath
          { _magicPathId = pMagicPathId (MagicPathId (tableField "id")),
            _name = tableField "name",
            _abbreviation = tableField "abbreviation"
          }
    )
