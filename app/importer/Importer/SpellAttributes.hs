module Importer.SpellAttributes (importSpellAttributes) where

import Control.Exception (throw)
import qualified Data.ByteString.Lazy as BL
import Data.Csv (NamedRecord, Parser, decodeByNameWithP, (.:))
import Data.Int (Int64)
import qualified Data.Vector as V
import Database.Model.Nation (NationIdT (NationId))
import Database.Model.NationSpell (NationSpell, NationSpellT (NationSpell))
import Database.Model.Spell (SpellId, SpellIdT (SpellId))
import Database.Mutations.NationSpell (deleteAll, insert)
import Database.PostgreSQL.Simple (Connection)
import Exception (ImporterException (..))
import Importer.Utils (tsvDecode)
import Opaleye (runDelete_, runInsert_)
import RawAttribute (RawAttribute (..), restrictToNation)
import System.FilePath ((</>))

type SpellAttributeTuple =
  ( SpellId,
    Int64,
    Int64
  )

currentFile :: FilePath
currentFile = "attributes_by_spell.csv"

valueParse :: NamedRecord -> Parser SpellAttributeTuple
valueParse r =
  (,,)
    <$> fmap SpellId (r .: "spell_number")
    <*> r .: "attribute"
    <*> r .: "raw_value"

recordToNationSpell :: SpellAttributeTuple -> NationSpell
recordToNationSpell row@(spellId, c, nationIdInt)
  | fromIntegral c == code restrictToNation = NationSpell spellId (NationId (nationIdInt - 100))
  | otherwise =
    throw
      DecodeException
        { file = currentFile,
          error = "Invalid call to recordToNationSpell, given row " ++ show row
        }

importSpellAttributes :: Connection -> (FilePath, FilePath) -> IO ()
importSpellAttributes c (_, gamedataDir) = do
  let file = gamedataDir </> currentFile
  csvData <- BL.readFile file
  let nationSpells = case decodeByNameWithP valueParse tsvDecode csvData of
        Left err -> throw $ DecodeException file err
        Right (_, v) ->
          V.toList <$> fmap recordToNationSpell $
            V.filter (\(_, attributeCode, _) -> attributeCode == fromIntegral (code restrictToNation)) v

  _ <- runDelete_ c deleteAll
  _ <- runInsert_ c $ insert nationSpells

  return ()
