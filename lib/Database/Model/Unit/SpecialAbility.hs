{-# LANGUAGE TypeFamilies #-}

module Database.Model.Unit.SpecialAbility where

import Control.Lens.TH
import qualified Data.Profunctor.Product.Default as D
import qualified Opaleye as O
import Opaleye.Experimental.Enum (fromFieldToFieldsEnum)
import Opaleye.Internal.Inferrable

data SpecialAbilityClass
  = Undead
  | Inanimate
  | Demon
  | Sailing
  deriving (Show, Eq, Ord)

data SpecialAbility = SpecialAbility
  { _klass :: SpecialAbilityClass,
    _requiredValues :: Int
  }
  deriving (Show, Eq)

$(makeLenses ''SpecialAbility)

data SqlSpecialAbility

toSqlAttributeString :: SpecialAbility -> String
toSqlAttributeString SpecialAbility {_klass = Undead} = "undead"
toSqlAttributeString SpecialAbility {_klass = Inanimate} = "inanimate"
toSqlAttributeString SpecialAbility {_klass = Demon} = "demon"
toSqlAttributeString SpecialAbility {_klass = Sailing} = "sailing"

fromSqlAttributeString :: String -> Maybe SpecialAbility
fromSqlAttributeString "undead" = Just (SpecialAbility Undead 0)
fromSqlAttributeString "inanimate" = Just (SpecialAbility Inanimate 0)
fromSqlAttributeString "demon" = Just (SpecialAbility Demon 0)
fromSqlAttributeString "sailing" = Just (SpecialAbility Sailing 2)
fromSqlAttributeString _ = Nothing

fromFieldAttribute :: O.FromField sqlAttribute SpecialAbility
toFieldsAttribute :: O.ToFields SpecialAbility (O.Column sqlEnum)
(fromFieldAttribute, toFieldsAttribute) =
  fromFieldToFieldsEnum "special_ability" fromSqlAttributeString toSqlAttributeString

instance O.DefaultFromField SqlSpecialAbility SpecialAbility where
  defaultFromField = fromFieldAttribute

instance
  attribute ~ SpecialAbility =>
  D.Default (Inferrable O.FromFields) (O.Column SqlSpecialAbility) SpecialAbility
  where
  def = Inferrable D.def

instance D.Default O.ToFields SpecialAbility (O.Column SqlSpecialAbility) where
  def = toFieldsAttribute
