module Importer.Utils where

import Data.Char (ord)
import Data.Csv (DecodeOptions, decDelimiter, defaultDecodeOptions)

tsvDecode :: DecodeOptions
tsvDecode =
  defaultDecodeOptions
    { decDelimiter = fromIntegral (ord '\t')
    }
