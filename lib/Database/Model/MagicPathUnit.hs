module Database.Model.MagicPathUnit where

import Control.Lens.TH
import Data.Int (Int32)
import Data.Profunctor.Product.TH (makeAdaptorAndInstance)
import qualified Database.Model.MagicPath as MP
import Database.Model.Unit.Utils
import Opaleye

data MagicPathUnitT sid mpid smallInt = MagicPathUnit
  { _unitId :: sid,
    _magicPathId :: mpid,
    _level :: smallInt
  }
  deriving (Show)

type MagicPathUnit =
  MagicPathUnitT UnitId MP.MagicPathId Int32

$(makeLenses ''MagicPathUnitT)
$(makeAdaptorAndInstance "pMagicPathUnit" ''MagicPathUnitT)

type MagicPathUnitField =
  MagicPathUnitT
    FieldUnitId
    MP.FieldMagicPathId
    (Field SqlInt4)

magicPathUnitTable :: Table MagicPathUnitField MagicPathUnitField
magicPathUnitTable =
  table
    "magic_path_unit"
    ( pMagicPathUnit
        MagicPathUnit
          { _unitId = pUnitId (UnitId (tableField "unit_id")),
            _magicPathId = MP.pMagicPathId (MP.MagicPathId (tableField "magic_path_id")),
            _level = tableField "level"
          }
    )

selectMagicPathUnits :: Select MagicPathUnitField
selectMagicPathUnits = selectTable magicPathUnitTable
