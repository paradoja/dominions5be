-- migrate:up
CREATE TYPE unit_attribute_type AS ENUM ('undead', 'inanimate', 'demon', 'sailing');

CREATE TABLE unit_attributes (
    unit_id integer
      NOT NULL
      REFERENCES units
      ON DELETE CASCADE,
    attribute unit_attribute_type NOT NULL,
    value1 integer,
    value2 integer,


    PRIMARY KEY (unit_id, attribute)
);


-- migrate:down

DROP TABLE unit_attributes;
DROP TYPE unit_attribute_type;
