-- migrate:up

ALTER TABLE unit_attributes RENAME COLUMN attribute TO special_ability;
ALTER TABLE unit_attributes RENAME TO unit_special_abilities;
ALTER TYPE unit_attribute_type RENAME TO special_ability;

-- migrate:down

ALTER TYPE special_ability RENAME TO unit_attribute_type;
ALTER TABLE unit_special_abilities RENAME COLUMN special_ability TO attribute;
ALTER TABLE unit_special_abilities RENAME TO unit_attributes;
