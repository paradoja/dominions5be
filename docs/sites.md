# Magic sites

## Fields in MagicSites.csv

|     | NAME                 | DESC                                                                                                                                |
| --- | -------------------- | ----------------------------------------------------------------------------------------------------------------------------------- |
| 1   | id                   | id                                                                                                                                  |
| 2   | name                 | name                                                                                                                                |
| 3   | rarity               | rarity                                                                                                                              |
| 4   | loc                  | sum of the values of location in site\*terrain_types.csv                                                                            |
| 5   | level                | level                                                                                                                               |
| 6   | path                 | path (name of path, not id)                                                                                                         |
| 7   | F                    | Gems                                                                                                                                |
| 8   | A                    | Gems                                                                                                                                |
| 9   | W                    | Gems                                                                                                                                |
| 10  | E                    | Gems                                                                                                                                |
| 11  | S                    | Gems                                                                                                                                |
| 12  | D                    | Gems                                                                                                                                |
| 13  | N                    | Gems                                                                                                                                |
| 14  | B                    | Gems                                                                                                                                |
| 15  | gold                 | brings gold                                                                                                                         |
| 16  | res                  | generates resources                                                                                                                 |
| 17  | sup                  | supply bonus                                                                                                                        |
| 18  | unr                  | unrest                                                                                                                              |
| 19  | exp                  | enter to gain xp                                                                                                                    |
| 20  | lab                  | "lab" if it builds a lab                                                                                                            |
| 21  | fort                 | number/strength of fort                                                                                                             |
| 22  | scale1               | increases scales                                                                                                                    |
| 23  | scale2               | second scale to increase, can be repeated                                                                                            |
| 24  | domspread            | dominion spread                                                                                                                     |
| 25  | turmoil              |                                                                                                                                     |
| 26  | sloth                |                                                                                                                                     |
| 27  | cold                 |                                                                                                                                     |
| 28  | death                |                                                                                                                                     |
| 29  | misfortune           |                                                                                                                                     |
| 30  | drain                |                                                                                                                                     |
| 31  | fireres              |                                                                                                                                     |
| 32  | coldres              |                                                                                                                                     |
| 33  | shockres             |                                                                                                                                     |
| 34  | poisonres            |                                                                                                                                     |
| 35  | str                  | strength                                                                                                                            |
| 36  | prec                 | precision                                                                                                                           |
| 37  | mor                  | morale                                                                                                                              |
| 38  | undying              | undying                                                                                                                             |
| 39  | att                  | attack                                                                                                                              |
| 40  | darkvision           | dark vision                                                                                                                         |
| 41  | aawe                 | animal awe                                                                                                                          |
| 42  | rit                  | magic path letters ritual (boosted by range)                                                                                        |
| 43  | ritrng               | ritual range                                                                                                                        |
| 44  | hmon1                | home \_units\*                                                                                                                      |
| 45  | hmon2                |                                                                                                                                     |
| 46  | hmon3                |                                                                                                                                     |
| 47  | hmon4                |                                                                                                                                     |
| 48  | hmon5                |                                                                                                                                     |
| 49  | voidgate             | summon void creatures                                                                                                               |
| 50  | sum1                 | summons                                                                                                                             |
| 51  | n\*sum1              | amount (i guess)                                                                                                                    |
| 52  | sum2                 |                                                                                                                                     |
| 53  | n_sum2               |                                                                                                                                     |
| 54  | sum3                 |                                                                                                                                     |
| 55  | n_sum3               |                                                                                                                                     |
| 56  | conj                 | conjuration bonus                                                                                                                   |
| 57  | alter                |                                                                                                                                     |
| 58  | evo                  |                                                                                                                                     |
| 59  | const                |                                                                                                                                     |
| 60  | ench                 |                                                                                                                                     |
| 61  | thau                 |                                                                                                                                     |
| 62  | blood                |                                                                                                                                     |
| 63  | heal                 | healing %                                                                                                                           |
| 64  | disease              |                                                                                                                                     |
| 65  | curse                | curses                                                                                                                              |
| 66  | horror               | horrormark chance                                                                                                                   |
| 67  | holyfire             | holy fire                                                                                                                           |
| 68  | holypow              | holy power                                                                                                                          |
| 69  | scry                 | enter to scry                                                                                                                       |
| 70  | adventure            | adventure                                                                                                                           |
| 71  | other                | empty                                                                                                                               |
| 72  | sum4                 |                                                                                                                                     |
| 73  | n_sum4               |                                                                                                                                     |
| 74  | hcom1                | home commander                                                                                                                      |
| 75  | hcom2                |                                                                                                                                     |
| 76  | hcom3                |                                                                                                                                     |
| 77  | hcom4                |                                                                                                                                     |
| 78  | hcom5                |                                                                                                                                     |
| 79  | mon1                 | unit                                                                                                                                |
| 80  | mon2                 |                                                                                                                                     |
| 81  | mon3                 |                                                                                                                                     |
| 82  | mon4                 |                                                                                                                                     |
| 83  | mon5                 |                                                                                                                                     |
| 84  | com1                 | commander                                                                                                                           |
| 85  | com2                 |                                                                                                                                     |
| 86  | com3                 |                                                                                                                                     |
| 87  | com4                 |                                                                                                                                     |
| 88  | com5                 |                                                                                                                                     |
| 89  | reveal               | reveal different graphs {0: 'mundane score graphs', 3: 'magic score graphs', 5: 'dominion score graphs', 999: 'all score graphs'}," |
| 90  | provdef1             | extra province defense \_units\*                                                                                                    |
| 91  | provdef2             |                                                                                                                                     |
| 92  | def                  | extra defense                                                                                                                       |
| 93  | F2                   | empty                                                                                                                               |
| 94  | A2                   | empty                                                                                                                               |
| 95  | W2                   | empty                                                                                                                               |
| 96  | E2                   | empty                                                                                                                               |
| 97  | S2                   | empty                                                                                                                               |
| 98  | D2                   | empty                                                                                                                               |
| 99  | N2                   | empty                                                                                                                               |
| 100 | B2                   | empty                                                                                                                               |
| 101 | awe                  | awe                                                                                                                                 |
| 102 | reinvigoration       | reinvigoration                                                                                                                      |
| 103 | airshield            | air shield                                                                                                                          |
| 104 | provdefcom           | empty                                                                                                                               |
| 105 | domconflict          | dominion conflict bonus                                                                                                             |
| 106 | sprite               | id for sprits                                                                                                                       |
| 107 | nationalrecruits     | nation                                                                                                                              |
| 108 | natmon               | _unit_                                                                                                                              |
| 109 | natcom               | _commander_                                                                                                                         |
| 110 | throneclustering     | throne clustering                                                                                                                   |
| 111 | wilddefenders        | wild defenders                                                                                                                      |
| 112 | domconflict          | repeated                                                                                                                            |
| 113 | rituallevelmodifier  | ritual level modifier                                                                                                               |
| 114 | callgodbonus         | call god bonus                                                                                                                      |
| 115 | magicresistancebonus | magic resistance bonus                                                                                                              |
| 116 | bringgold            | brings gold, different from the previous                                                                                            |
| 117 | scorch               | scorching desert "AN fire damage"                                                                                                   |
