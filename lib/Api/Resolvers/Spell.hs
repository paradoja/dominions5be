module Api.Resolvers.Spell (spellsR) where

import Api.Resolvers.MagicPath (magicPathLevelToGQL)
import qualified Api.Resolvers.Nation as RN
import Api.Resolvers.Utils
import Api.Schema
import Data.Int (Int32)
import Data.Maybe (isNothing)
import Data.Morpheus.Types hiding (description)
import Data.Text (pack)
import Database.Fetch
import qualified Database.Model as M
import Datasource

type DBSpell = (M.Spell, Maybe M.MagicSchoolLevel, [M.MagicPathLevel], [M.Nation])

gemAmountToGQL :: M.MagicPath -> Int32 -> GemAmount (MQueryF a)
gemAmountToGQL M.MagicPath {_magicPathId = M.MagicPathId magicPathId, _name} amount =
  GemAmount
    { magicPath =
        pure
          MagicPath
            { id = pure $ ID (pack $ show magicPathId),
              name = pure _name
            },
      amount = pure $ fromIntegral amount
    }

magicSchoolLeveltoGQL :: M.MagicSchoolLevel -> MagicSchoolLevel (MQueryF a)
magicSchoolLeveltoGQL (M.MagicSchool {magicSchoolId = M.MagicSchoolId magicSchoolId, name}, level) =
  MagicSchoolLevel
    { magicSchool =
        pure
          MagicSchool
            { id = pure $ ID (pack $ show magicSchoolId),
              name = pure name
            },
      level = pure $ fromIntegral level
    }

toGQL :: DBSpell -> Spell (MQueryF a)
toGQL
  ( M.Spell {spellId = M.SpellId spellId, name, fatigueCost, gemCost, damage, precision, description, details},
    mssM,
    mpsL,
    nations
    ) =
    Spell
      { id = pure $ ID (pack $ show spellId),
        name = pure name,
        fatigueCost = pure $ fromIntegral fatigueCost,
        gemCost = pure $ toGemAmount (fmap fst mpsL),
        damage = pure $ fromIntegral damage,
        precision = pure $ fromIntegral precision,
        description = pure description,
        details = pure details,
        magicSchoolLevel = pure $ magicSchoolLeveltoGQL <$> mssM,
        magicPathLevels = pure $ fmap magicPathLevelToGQL mpsL,
        internal = pure $ isNothing mssM,
        restrictedToNations = pure $ fmap RN.toGQL nations
      }
    where
      toGemAmount [] = Nothing
      toGemAmount (ms : _) = Just $ gemAmountToGQL ms gemCost

spellsR :: SpellsArgs -> ResolverQ e Haxl [Spell (MQueryF ())]
spellsR SpellsArgs {name, magicSchool, restrictedToNation, includeInternal} =
  liftEither $ do
    let eitherId = sequence $ idFromGraphQL <$> restrictedToNation
    either
      (return . Left)
      ( \nidM -> do
          fs <- getFullSpells (M.NationId <$> nidM) name magicSchool includeInternal
          return . Right $ toGQL <$> fs
      )
      eitherId
