module Database.Model.MagicPathSpell where

import Data.Int (Int32)
import Data.Profunctor.Product.TH (makeAdaptorAndInstance)
import Database.Model.MagicPath
import Database.Model.Spell
import Opaleye

data MagicPathSpellT sid mpid smallInt = MagicPathSpell
  { spellId :: sid,
    magicPathId :: mpid,
    level :: smallInt,
    sorting :: smallInt
  }
  deriving (Show)

type MagicPathSpell =
  MagicPathSpellT SpellId MagicPathId Int32

$(makeAdaptorAndInstance "pMagicPathSpell" ''MagicPathSpellT)

type MagicPathSpellField =
  MagicPathSpellT
    FieldSpellId
    FieldMagicPathId
    (Field SqlInt4)

magicPathSpellTable :: Table MagicPathSpellField MagicPathSpellField
magicPathSpellTable =
  table
    "magic_path_spell"
    ( pMagicPathSpell
        MagicPathSpell
          { spellId = pSpellId (SpellId (tableField "spell_id")),
            magicPathId = pMagicPathId (MagicPathId (tableField "magic_path_id")),
            level = tableField "level",
            sorting = tableField "sorting"
          }
    )

selectMagicPathSpells :: Select MagicPathSpellField
selectMagicPathSpells = selectTable magicPathSpellTable
