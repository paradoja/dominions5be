module Database.Fetch.Unit where

import Data.Text (Text)
import Database.Fetch.Utils
import Database.Model.Unit

getBasicUnitById :: UnitId -> Haxl BasicUnit
getBasicUnitById = dataFetch . FetchBasicUnit

getBasicUnitsForName :: Maybe Text -> Haxl [BasicUnit]
getBasicUnitsForName = dataFetch . FetchBasicUnits

unitFromBasicUnit :: BasicUnit -> Haxl Unit
unitFromBasicUnit u@BasicUnit {_unitAttributes = BasicUnitAttributes {_unitId}} = do
  specialAbilities <- dataFetch $ FetchUnitSpecialAbilities _unitId
  magicPathLevels <- dataFetch $ FetchUnitMagicPathLevels _unitId
  return $ toUnit u specialAbilities magicPathLevels
