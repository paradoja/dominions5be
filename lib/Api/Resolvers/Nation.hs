module Api.Resolvers.Nation (nationsR, nationR, toGQL) where

import Api.Resolvers.Utils
import Api.Schema
import Data.Morpheus.Types
import Data.Text (pack)
import Database.Fetch
import qualified Database.Model as M
import Datasource
import Haxl.Core (catch)

fromGQLEra :: Era -> M.Era
fromGQLEra EARLY = M.Early
fromGQLEra MIDDLE = M.Middle
fromGQLEra LATE = M.Late

toGQLEra :: M.Era -> Era
toGQLEra M.Early = EARLY
toGQLEra M.Middle = MIDDLE
toGQLEra M.Late = LATE

toGQL :: M.Nation -> Nation (MQueryF a)
toGQL M.Nation {nationId = M.NationId nationId, name, epithet, abbreviation, era} =
  Nation
    { id = pure $ ID (pack $ show nationId),
      name = pure name,
      epithet = pure epithet,
      abbreviation = pure abbreviation,
      era = pure $ toGQLEra era
    }

nationsR :: NationsArgs -> ResolverQ e Haxl [Nation (MQueryF ())]
nationsR NationsArgs {era} = lift $ fmap toGQL <$> getNationsByEra (fromGQLEra <$> era)

nationR :: NationArgs -> ResolverQ e Haxl Nation
nationR NationArgs {id = idM} =
  liftEither $
    either
      (return . Left)
      ( \nid ->
          fmap (Right . toGQL) (getNationById (M.NationId nid))
            `catch` ( \(_ :: DBNotFound) ->
                        return . Left $
                          concat ["Nation with id ", show nid, " not found"]
                    )
      )
      (idFromGraphQL idM)
