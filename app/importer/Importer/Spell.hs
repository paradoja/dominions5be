module Importer.Spell (importSpells) where

import Control.Exception (IOException, catch, throw)
import qualified Data.ByteString.Lazy as BL
import Data.Char (isLetter, toLower)
import Data.Csv
import Data.Int (Int32, Int64)
import Data.List (find)
import Data.List.Split (wordsBy)
import Data.Maybe (catMaybes)
import Data.Text (Text, strip, unpack)
import qualified Data.Text.IO as TextIO
import qualified Data.Vector as V
import Database.Model.MagicPath (MagicPathId, MagicPathIdT (..))
import Database.Model.MagicPathSpell (MagicPathSpell, MagicPathSpellT (..))
import Database.Model.MagicSchool (MagicSchoolId, MagicSchoolIdT (..))
import Database.Model.MagicSchoolSpell (MagicSchoolSpell, MagicSchoolSpellT (..))
import Database.Model.Spell (Spell, SpellId, SpellIdT (..), SpellT (..))
import Database.Mutations.MagicPathSpell as MPS
import Database.Mutations.MagicSchoolSpell as MSS
import Database.Mutations.Spell as SP
import Database.PostgreSQL.Simple (Connection)
import Exception
import Importer.Utils
import Opaleye (runDelete_, runInsert_)
import System.Directory (listDirectory)
import System.FilePath ((</>))

type SpellTuple =
  ( SpellId,
    Text,
    MagicSchoolId,
    Int32,
    MagicPathId,
    Int32,
    MagicPathId,
    Int32,
    Int32,
    Int32,
    Int64,
    Int32
  )

valueParse :: NamedRecord -> Parser SpellTuple
valueParse r =
  (,,,,,,,,,,,)
    <$> fmap SpellId (r .: "id")
    <*> r .: "name"
    <*> fmap MagicSchoolId (r .: "school")
    <*> r .: "researchlevel"
    <*> fmap MagicPathId (r .: "path1")
    <*> r .: "pathlevel1"
    <*> fmap MagicPathId (r .: "path2")
    <*> r .: "pathlevel2"
    <*> r .: "fatiguecost"
    <*> r .: "gemcost"
    <*> r .: "damage"
    <*> r .: "precision"

recordToObjects ::
  Maybe Text ->
  Maybe Text ->
  SpellTuple ->
  ( Spell,
    Maybe MagicSchoolSpell,
    [MagicPathSpell]
  )
recordToObjects
  descM
  detailM
  ( spellId,
    name,
    school,
    researchLevel1,
    path1,
    pathLevel1,
    path2,
    pathLevel2,
    fatigueCost,
    gemCost,
    damage,
    precision
    ) =
    ( Spell spellId name fatigueCost gemCost damage precision descM detailM,
      maybeSchool spellId school researchLevel1,
      catMaybes
        [ maybePath spellId path1 pathLevel1 1,
          maybePath spellId path2 pathLevel2 2
        ]
    )
    where
      maybeSchool _ (MagicSchoolId (-1)) _ = Nothing
      maybeSchool spId msId level =
        Just $ MagicSchoolSpell spId msId level
      maybePath _ (MagicPathId (-1)) _ _ = Nothing
      maybePath spId mpId level sorting =
        Just $ MagicPathSpell spId mpId level sorting

importSpells :: Connection -> (FilePath, FilePath) -> IO ()
importSpells c (_, gamedataDir) = do
  let file = gamedataDir </> "spells.csv"
  let descrPath = gamedataDir </> "spelldescr/"
  descs <- listDirectory descrPath
  csvData <- BL.readFile file
  objects <-
    case decodeByNameWithP valueParse tsvDecode csvData of
      Left err -> throw $ DecodeException file err
      Right (_, v) -> fmap V.toList $
        V.forM v $ \r -> do
          (descM, detailsM) <- getDescriptionAndDetails descrPath descs r
          return (recordToObjects descM detailsM r)
  let (spells, schoolsM, pathsLists) = unzip3 objects
  let schools = catMaybes schoolsM
  let paths = concat pathsLists

  _ <- runDelete_ c MSS.deleteAll
  _ <- runDelete_ c MPS.deleteAll
  _ <- runDelete_ c SP.deleteAll
  _ <- runInsert_ c $ SP.insert spells
  _ <- runInsert_ c $ MSS.insert schools
  _ <- runInsert_ c $ MPS.insert paths
  return ()

-- We could get the directory listing here, instead of on the calling
-- function but this way we cache the results for each iteration
getDescriptionAndDetails :: FilePath -> [FilePath] -> SpellTuple -> IO (Maybe Text, Maybe Text)
getDescriptionAndDetails dir descFiles tuple =
  let cName = canonName tuple
      descFileNameM = find ((== fmap toLower cName) . fmap toLower) descFiles
      descFilePathM =
        -- sometimes the case of the file is wrong :S
        (dir </>) <$> descFileNameM
      detailsFilePathM = ((dir </> "details") ++) <$> descFileNameM
   in maybe
        (return (Nothing, Nothing))
        ( \f -> do
            descM <- readfileM f
            detailsM <- maybe (return Nothing) readfileM detailsFilePathM
            return (descM, detailsM)
        )
        descFilePathM
  where
    canonName (_, name, _, _, _, _, _, _, _, _, _, _) =
      concat (wordsBy (not . isLetter) $ unpack name) ++ ".txt"
    readfileM f =
      (cleanContents <$> TextIO.readFile f)
        `catch` (\(_ :: IOException) -> return Nothing)
    cleanContents text =
      if strip text == ""
        then Nothing
        else Just text
