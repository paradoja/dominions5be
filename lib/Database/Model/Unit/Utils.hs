module Database.Model.Unit.Utils where

import Data.Hashable
import Data.Int (Int64)
import Data.Profunctor.Product.TH (makeAdaptorAndInstance)
import Opaleye (Field)
import Opaleye.SqlTypes (SqlInt8)

newtype UnitIdT a = UnitId a
  deriving (Show, Eq, Ord)

type UnitId = UnitIdT Int64

type FieldUnitId = UnitIdT (Field SqlInt8)

$(makeAdaptorAndInstance "pUnitId" ''UnitIdT)

instance Hashable UnitId where
  hashWithSalt s (UnitId a) = hashWithSalt s a
