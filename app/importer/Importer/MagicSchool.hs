{-# OPTIONS_GHC -fno-warn-orphans #-}

module Importer.MagicSchool where

import Data.Csv
import Database.Model.MagicSchool (MagicSchool, MagicSchoolIdT (..), MagicSchoolT (..))

instance FromNamedRecord MagicSchool where
  parseNamedRecord r =
    MagicSchool
      <$> fmap MagicSchoolId (r .: "number")
      <*> r .: "name"
