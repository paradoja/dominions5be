# dominions5be

[[_TOC_]]

Dominions 5 game data GraphQL server.

## Doc on current work

At [docs/README.md](docs/README.md) you can get information on the
current efforts or problems.

## Daily running

Everything should be set up and the project built.

### Building (with file watch)

```
stack build --file-watch
```

If the `schema.graphql` is changed, the file watch has to be stopped
(^C) and re-run.

### DB migrations

New migration:

```
dbmate new name_of_migration
```

Now in the `db/migrations` directory a new migration file will be
created.

Apply with:

```
db up
```

If you need to rollback a migration just do:

```
db rollback
```

The data can always be re-imported, so no problems doing this.

### Import (& clean) data

```
stack exec -- dominions5-import
```

### Run server

```
stack exec -- dominions5-be
```

#### GraphQL client

Once the server is running you have a GraphQL client in `/graphql` (so
in dev, [`localhost:3000/graphql`](https://localhost:3000/graphql) by
default.

## Initial setup

### Tools for dev.

- You *need* to install [`stack`](https://docs.haskellstack.org/). That
  should take care of most of the tooling.
- You *need* to install [`dbmate`](https://github.com/amacneil/dbmate)
  is used for DB creation and migrations.
- You *need* Postgres.
- You may want to use `direnv` or something similar to load easily env
  variables.
- You may want to install
  [`yarn`](https://yarnpkg.com/getting-started) or of course
  `npm`. They are used to install a couple of extra CLI utilities (but
  most of the time they are not needed at all).

### Ubuntu (and maybe other Linux/whatever distros)

For libraries to compile you need `libpq-dev`.

### Repo setup && resource update

The project includes a submodule to the [Dominions 5
Inspector](https://github.com/larzm42/dom5inspector/), so when cloning
the project, do it with `git clone --recurse-submodules` _or_ after
normal cloning do:

```git submodule update --init```

After that, set up the DB.

### DB configuration & setup

The configuration for the DB in development by default is in a `.env`
file. If you go to [`.env-default`](.env-default), you'll see a
template to add your local configuration.

#### New DB user

To create a new user for the project you can do:
```
createuser dominions5be --pwprompt --superuser
# Enter a password for project when prompted
```

Notice that this creates an admin user to your DB for
convenience. Only use it in a dev. machine if you are comfortable with
this.

#### DB env file

Now, you can `cp .env-default .env` and then at `.env` fill up the
data for the DB you created. This file is in the gitignore to avoid
pushing it.

#### Env variables

To run the binaries, we need the DB data in the env. variables. The
template shows the variables. If you use `direnv` it can take care of
that (we include a `.envrc` file for this in
particular). Alternatively you can load the variables some other way,
of course.

#### Create DB & initial migration

You then can create the DB and apply the migrations with:

```
dbmate create
dbmate up
```

### Build project and initialize data

Now you can build the project and run the importer:

```
stack build
stack exec dominions5-import
```

## Deployment

Right now the deployment system is very basic.

- [`make-bundle`](deploy/make-bundle) is a script that builds a basic
  [Keter](https://github.com/snoyberg/keter/) application bundle for
  the server.

- The [`deploy`](deploy/deploy) script is will call `make-bundle`,
  `rsync` the server to the standard `incoming` folder in a host, and
  will migrate and import data from a directory where the repository
  is already uploaded and set up. It is not ideal, but works for
  now. It requires:

  - a `REMOTE_HOST` env variable with the remote host,
  - and a `REMOTE_PATH` env variable with the path to the project in
    the host.

## Project organisation

To be written (sorry)

## Other utils

### Command line GraphQL tools

For convenience you can install [GraphQL
Inspector](https://www.npmjs.com/package/@graphql-inspector/cli) from
the project root:

```
yarn install
```

This will install the CLI tool in
`node_modules/.bin/graphql-inspector`. You can run it from there or
put that directory in the path (`direnv` helps with that, as several
shell plugins).

#### Serve/validate

```graphql-inspector serve schema.graphql```

will serve a fake server for the schema, and also, nicely validate the
schema.
