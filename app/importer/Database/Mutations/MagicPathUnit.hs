module Database.Mutations.MagicPathUnit where

import Data.Int (Int64)
import Database.Model.MagicPathUnit
  ( MagicPathUnit,
    magicPathUnitTable,
  )
import Opaleye
  ( Delete (..),
    Insert (..),
    iOnConflict,
    rCount,
  )
import Opaleye.ToFields (toFields)

insert :: [MagicPathUnit] -> Insert Int64
insert mpss =
  Insert
    { iTable = magicPathUnitTable,
      iRows = map toFields mpss,
      iReturning = rCount,
      iOnConflict = Nothing
    }

deleteAll :: Delete Int64
deleteAll =
  Delete
    { dTable = magicPathUnitTable,
      dWhere = const $ toFields True,
      dReturning = rCount
    }
