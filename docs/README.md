# Current work on:

Internal links to .js or .csv files are to files in the dom5inspector
repo.

- [spells](spells.md)
- [nations](nations.md)
- [units](units.md)
- [sites](sites.md)


## General info

- [`scripts/parsemod.js`](https://github.com/larzm42/dom5inspector/blob/gh-pages/scripts/parsemod.js)
information on how things are parsed.

- `gamedata/attribute_keys.csv` has information on attributes (eg. restricted by Nation)
