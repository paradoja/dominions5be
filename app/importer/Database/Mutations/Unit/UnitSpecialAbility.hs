module Database.Mutations.Unit.UnitSpecialAbility where

import Data.Int (Int64)
import Database.Model.Unit
import Opaleye
  ( Delete (..),
    Insert (..),
    iOnConflict,
    rCount,
  )
import Opaleye.ToFields (toFields)

insert :: [UnitSpecialAbility] -> Insert Int64
insert objs =
  Insert
    { iTable = unitSpecialAbilitiesTable,
      iRows = map toFields objs,
      iReturning = rCount,
      iOnConflict = Nothing
    }

deleteAll :: Delete Int64
deleteAll =
  Delete
    { dTable = unitSpecialAbilitiesTable,
      dWhere = const $ toFields True,
      dReturning = rCount
    }
